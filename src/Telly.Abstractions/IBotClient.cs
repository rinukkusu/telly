using System;
using System.IO;
using System.Threading.Tasks;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;

namespace Telly.Abstractions
{
    public interface IBotClient
    {
        public event EventHandler<Message> OnMessage;
        public bool IsCommand(Message message);
        public string CommandPrefix { get; }
        public dynamic BotOwnerId { get; }
        public Task SendTextAsync(Message originalMessage, IEntity target, string text, ParseMessageAs parseMessageAs = ParseMessageAs.Default, dynamic replyToMessageId = null, bool disablePreview = false);
        public Task SendDataAsync(Message originalMessage, IEntity target, byte[] data, DataType type, dynamic replyToMessageId = null);
        public Task SendDataAsync(Message originalMessage, IEntity target, Stream data, DataType type, dynamic replyToMessageId = null);
        public Task SendDataFromUrlAsync(Message originalMessage, IEntity target, string dataUrl, DataType type, dynamic replyToMessageId = null);
        public Task SendDataCollectionFromUrlsAsync(Message originalMessage, IEntity target, string[] dataUrls, DataType type, dynamic replyToMessageId = null);
    }

    public abstract class BotClientBase : IBotClient
    {
        public abstract string CommandPrefix { get; }
        public abstract dynamic BotOwnerId { get; }

        public bool IsCommand(Message message) => message.Text != null && message.Text.StartsWith(CommandPrefix);

        public async Task SendDataAsync(Message originalMessage, IEntity target, byte[] data, DataType type, dynamic replyToMessageId = null)
        {
            await using var memStream = new MemoryStream(data);
            await SendDataAsync(originalMessage, target, memStream, type, replyToMessageId);
        }

        public abstract Task Start();
        public abstract event EventHandler<Message> OnMessage;
        public abstract Task SendTextAsync(Message originalMessage, IEntity target, string text, ParseMessageAs parseMessageAs = ParseMessageAs.Default,  dynamic replyToMessageId = null, bool disablePreview = false);
        public abstract Task SendDataAsync(Message originalMessage, IEntity target, Stream data, DataType type, dynamic replyToMessageId = null);
        public abstract Task SendDataFromUrlAsync(Message originalMessage, IEntity target, string dataUrl, DataType type, dynamic replyToMessageId = null);
        public abstract Task SendDataCollectionFromUrlsAsync(Message originalMessage, IEntity target, string[] dataUrls, DataType type, dynamic replyToMessageId = null);
    }
}