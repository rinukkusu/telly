namespace Telly.Abstractions.Enums
{
    public enum ParseMessageAs
    {
        Default,
        Markdown,
        Html
    }
}