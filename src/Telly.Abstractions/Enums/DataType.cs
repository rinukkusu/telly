namespace Telly.Abstractions.Enums
{
    public enum DataType
    {
        File,
        Photo,
        Audio,
        Video
    }
}