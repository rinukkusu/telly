using System;
using System.Collections.Generic;

namespace Telly.Abstractions.Models
{
    public class Message
    {
        public string Platform { get; set; }
        public dynamic Id { get; set; }
        public DateTime Date { get; set; }
        public User Sender { get; set; }
        public IEntity Target { get; set; }
        public string Text { get; set; }
        public bool HasMentionedBot { get; set; }
        public IList<dynamic> Mentions { get; set; } = new List<dynamic>();
    }
}
