using Telly.Abstractions.Enums;

namespace Telly.Abstractions.Models
{
    public interface IHandlerResult
    {
        bool ReplyToOriginalMessage { get; }
    }

    public class EmptyResult : IHandlerResult
    {
        public bool ReplyToOriginalMessage { get; } = false;
    }

    public class TextResult : IHandlerResult
    {
        public bool ReplyToOriginalMessage { get; }
        public ParseMessageAs ParseMessageAs { get; }
        public string Text { get; }

        public TextResult(string text, 
            ParseMessageAs parseMessageAs = ParseMessageAs.Default, bool replyToOriginalMessage = false)
        {
            Text = text;
            ParseMessageAs = parseMessageAs;
            ReplyToOriginalMessage = replyToOriginalMessage;
        }
    }

    public class DataResult : IHandlerResult
    {
        public bool ReplyToOriginalMessage { get; }
        public string Text { get; }
        public byte[] Data { get; }
        public DataType Type { get; }

        public DataResult(byte[] data, DataType type, string text = null, bool replyToOriginalMessage = false)
        {
            Data = data;
            Type = type;
            Text = text;
            ReplyToOriginalMessage = replyToOriginalMessage;
        }
    }

    public class DataFromUrlResult : IHandlerResult
    {
        public bool ReplyToOriginalMessage { get; }
        public string Text { get; }
        public string DataUrl { get; }
        public DataType Type { get; }

        public DataFromUrlResult(string dataUrl, DataType type, string text = null, bool replyToOriginalMessage = false)
        {
            DataUrl = dataUrl;
            Type = type;
            Text = text;
            ReplyToOriginalMessage = replyToOriginalMessage;
        }
    }

    public class DataCollectionsFromUrlsResult : IHandlerResult
    {
        public bool ReplyToOriginalMessage { get; }
        public string Text { get; }
        public string[] DataUrls { get; }
        public DataType Type { get; }

        public DataCollectionsFromUrlsResult(string[] dataUrls, DataType type, string text = null, bool replyToOriginalMessage = false)
        {
            DataUrls = dataUrls;
            Type = type;
            Text = text;
            ReplyToOriginalMessage = replyToOriginalMessage;
        }
    }
}
