using System.Linq;

namespace Telly.Abstractions.Models
{
    public interface IArgument
    {
        string Name { get; }
        bool IsOptional { get; }
        dynamic Parse(string argument);
    }
    
    public abstract class ArgumentBase : IArgument
    {
        public string Name { get; }
        public bool IsOptional { get; }
        public virtual dynamic Parse(string argument) => argument;

        public ArgumentBase(string name, bool optional = false)
        {
            Name = name;
            IsOptional = optional;
        }
    }

    public class TextArgument : ArgumentBase, IArgument
    {
        public TextArgument(string name, bool optional = false) : base(name, optional)
        {
        }
    }

    public class CatchAllArgument : ArgumentBase, IArgument
    {
        public CatchAllArgument(string name, bool optional = false) : base(name, optional)
        {
        }
    }

    public class IntArgument : ArgumentBase, IArgument
    {
        public IntArgument(string name, bool optional = false) : base(name, optional)
        {
        }

        public override dynamic Parse(string argument) => int.Parse(argument);
    }

    public class ListArgument : ArgumentBase, IArgument
    {
        private readonly string _separator;
        
        public ListArgument(string name, string separator, bool optional = false) : base(name, optional)
        {
            _separator = separator;
        }

        public override dynamic Parse(string argument)
        {
            return argument.Split(_separator).ToList();
        }
    }
}