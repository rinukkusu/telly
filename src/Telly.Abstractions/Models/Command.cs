using System;
using System.Text.RegularExpressions;

namespace Telly.Abstractions.Models
{
    public class Command {
        private static readonly Regex CommandRegex = new Regex(@"^.(?<command>.+?)(?<botname>@[^\s]+)?(\s(?<args>.+))?$");
        public string Name { get; }
        public string Arguments { get; }
        public Message OriginalMessage { get; }

        private Command(string name, string arguments, Message originalMessage)
        {
            Name = name;
            Arguments = arguments;
            OriginalMessage = originalMessage;
        }

        public static Command FromMessage(Message message) {
            var match = CommandRegex.Match(message.Text);
            var commandName = match.Groups["command"].Value;
            var botname = match.Groups["botname"].Value;
            var arguments = match.Groups["args"].Value;

            Console.WriteLine($"Command: {commandName} | Botname: {botname} | Args: {string.Join(", ", arguments)}");
        
            // try {
            //     var alias = AliasService.Instance.Get(commandName);
            //     match = commandRegex.Match(alias);
            //     commandName = match.Groups["command"].Value;
            //     arguments = match.Groups["args"].Value + (string.IsNullOrWhiteSpace(arguments) ? string.Empty : " " + arguments);
            // }
            // catch(KeyNotFoundException ex) {  }
        
            return new Command(commandName, arguments, message);
        }
    }
}
