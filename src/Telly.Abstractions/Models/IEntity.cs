namespace Telly.Abstractions.Models
{
    public interface IEntity
    {
        dynamic Id { get; set; }
        string Name { get; set; }
    }

    public class User : IEntity
    {
        public dynamic Id { get; set; }
        public string Name { get; set; }
    }
    
    public class Chat : IEntity
    {
        public dynamic Id { get; set; }
        public string Name { get; set; }
    }
}