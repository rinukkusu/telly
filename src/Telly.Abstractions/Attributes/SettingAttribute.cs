using System;

namespace Telly.Abstractions.Attributes
{
    public class SettingAttribute : Attribute
    {
        public string[] NodePath { get; }
        public string File { get; set; }

        public SettingAttribute(params string[] nodePath)
        {
            NodePath = nodePath;
        }
    }

    public class ManagedSettingAttribute : Attribute
    {
        public string File { get; }

        public ManagedSettingAttribute(string file)
        {
            File = file;
        }
    }
}