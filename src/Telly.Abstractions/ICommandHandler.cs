﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Telly.Abstractions.Models;

namespace Telly.Abstractions
{
    public interface ICommandHandler
    {
        string CommandName { get; }
        string Description { get; }

        IList<IArgument> Arguments { get; }

        bool IsMatch(Command command);

        Task<IHandlerResult> Handle(IBotClient client, Command command);
    }
}