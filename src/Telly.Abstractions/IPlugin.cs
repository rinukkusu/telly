using System;
using Microsoft.Extensions.DependencyInjection;

namespace Telly.Abstractions
{
    public interface IPlugin
    {
        public IServiceCollection Register(IServiceCollection services);
    }

    public abstract class PluginBase : IPlugin
    {
        public abstract IServiceCollection Register(IServiceCollection services);
    }

    public interface IPluginHost
    {
        public event EventHandler OnStart;
        public event EventHandler OnStop;
    }
}