namespace Telly.Abstractions
{
    public interface IPluginSettings
    {
        public bool IsDisabled { get; set; }
    }
}