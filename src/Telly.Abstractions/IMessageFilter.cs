using System.Threading.Tasks;
using Telly.Abstractions.Models;

namespace Telly.Abstractions
{
    public interface IMessageFilter
    {
        Task<Message> Execute(IBotClient client, Message message);
    }
}