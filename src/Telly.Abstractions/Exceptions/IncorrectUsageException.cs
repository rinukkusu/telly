using System;

namespace Telly.Abstractions.Exceptions
{
    public class IncorrectUsageException : Exception
    {
        public IncorrectUsageException()
        {
        }
    }
}