using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Telly.Abstractions
{
    public class SettingsManager<TSettings> 
        where TSettings : class, new()
    {
        private readonly string _filePath;
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

        public SettingsManager(string filePath)
        {
            _filePath = filePath;
        }

        public async Task<TSettings> Load()
        {
            try
            {
                await _semaphore.WaitAsync();
                
                if (!File.Exists(_filePath))
                    return new TSettings();

                var json = await File.ReadAllTextAsync(_filePath);
                return JsonConvert.DeserializeObject<TSettings>(json);
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public async Task Save(TSettings settings)
        {
            try
            {
                await _semaphore.WaitAsync();
                
                var json = JsonConvert.SerializeObject(settings, Formatting.Indented);
                await File.WriteAllTextAsync(_filePath, json);
            }
            finally
            {
                _semaphore.Release();
            }
        }
    }
}