using Telly.Abstractions.Attributes;

namespace Telly.Bot.Infrastructure
{
    [Setting("System")]
    public class SystemSettings
    {
        public string PluginsPath { get; set; }
        public string Localization { get; set; }
    }
}