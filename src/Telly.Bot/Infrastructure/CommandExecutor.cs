using System.Collections.Generic;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;
using Telly.Plugins.System;

namespace Telly.Bot.Infrastructure
{
    public class CommandExecutor
    {
        private readonly IEnumerable<ICommandHandler> _handlers;

        public CommandExecutor(IEnumerable<ISystemCommandHandler> systemHandlers, IEnumerable<ICommandHandler> handlers)
        {
            var combined = new List<ICommandHandler>(systemHandlers);
            combined.AddRange(handlers);
            _handlers = combined;
        }

        public async Task Execute(IBotClient botClient, Command command)
        {
            foreach (var handler in _handlers)
            {
                if (!handler.IsMatch(command))
                    continue;

                var result = await handler.Handle(botClient, command);

                var originalMessageId = result.ReplyToOriginalMessage
                    ? command.OriginalMessage.Id
                    : null;

                switch (result)
                {
                    case DataResult dataResult:
                        await botClient.SendDataAsync(command.OriginalMessage,
                            command.OriginalMessage.Target,
                            dataResult.Data,
                            dataResult.Type,
                            originalMessageId);
                        break;
                    
                    case TextResult textResult:
                        await botClient.SendTextAsync(command.OriginalMessage,
                            command.OriginalMessage.Target, 
                            textResult.Text, 
                            textResult.ParseMessageAs,
                            originalMessageId);
                        break;

                    case DataCollectionsFromUrlsResult dataResult:
                        await botClient.SendDataCollectionFromUrlsAsync(command.OriginalMessage,
                            command.OriginalMessage.Target,
                            dataResult.DataUrls,
                            dataResult.Type,
                            originalMessageId);
                        break;

                    case EmptyResult emptyResult:
                    default:
                        break;
                }
            }
        }
    }
}
