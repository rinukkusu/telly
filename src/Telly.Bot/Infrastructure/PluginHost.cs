using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Telly.Abstractions;

namespace Telly.Bot.Infrastructure
{
    public class PluginHost : IPluginHost
    {
        public event EventHandler OnStart = (o, e) => {};
        public event EventHandler OnStop = (o, e) => {};

        private readonly SystemSettings _settings;
        private IList<IPlugin> _plugins = new List<IPlugin>();

        public PluginHost(SystemSettings settings)
        {
            _settings = settings;
        }

        private void LoadPlugins()
        {
            var pluginPaths = Directory.EnumerateFiles(_settings.PluginsPath, "*.dll")
                .Where(x => x.EndsWith("Plugin.dll"))
                .ToList();

            _plugins = pluginPaths
                .Select(Assembly.LoadFile)
                .Select(assembly => assembly.DefinedTypes
                    .Where(x => (typeof(IPlugin)).IsAssignableFrom(x.AsType()))
                    .ToList())
                .SelectMany(definedPlugins => definedPlugins
                    .Select(definedPlugin => (IPlugin) Activator.CreateInstance(definedPlugin)))
                .ToList();
        }

        public IServiceCollection RegisterPlugins(IServiceCollection services)
        {
            foreach (var plugin in _plugins)
            {
                plugin.Register(services);
            }

            return services;
        }
    }
}