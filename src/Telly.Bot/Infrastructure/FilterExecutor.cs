using System.Collections.Generic;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;

namespace Telly.Bot.Infrastructure
{
    public class FilterExecutor
    {
        private readonly IEnumerable<IMessageFilter> _filters;

        public FilterExecutor(IEnumerable<IMessageFilter> filters)
        {
            _filters = filters;
        }

        public async Task<Message> Execute(IBotClient botClient, Message message)
        {
            foreach (var messageFilter in _filters)
            {
                message = await messageFilter.Execute(botClient, message);
            }

            return message;
        }
    }
}