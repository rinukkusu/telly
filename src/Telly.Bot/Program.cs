using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using Telly.Bot.Infrastructure;
using Telly.Clients.IRC;
using Telly.Clients.Telegram;
using Telly.Extensions;
using Telly.Plugins.Booru;
using Telly.Plugins.Cleverbot;
using Telly.Plugins.CoinmarketCap;
using Telly.Plugins.Corona.Austria;
using Telly.Plugins.FinnHub;
using Telly.Plugins.IexCloud;
using Telly.Plugins.Summarize;
using Telly.Plugins.System;
using Telly.Plugins.TelegramSearch;
using Telly.Plugins.Weather;
using Telly.Plugins.WolframAlpha;
using Telly.Plugins.YouTube;

namespace Telly.Bot
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddEnvironmentVariables()
                .AddJsonFile("appsettings.json")
                .Build();

            var services = new ServiceCollection()
                .AddSystemPlugin()
                .AddTelegram(configuration)
                //.AddIrc(configuration)
                .AddSettings<SystemSettings>(configuration)
                .AddSingleton<IPluginHost, PluginHost>()
                .AddSingleton<FilterExecutor>()
                .AddSingleton<CommandExecutor>()
                .AddYouTubePlugin(configuration)
                .AddBooruPlugin(configuration)
                .AddCoinmarketCapPlugin(configuration)
                .AddWeatherPlugin(configuration)
                .AddCoronaAustriaPlugin()
                .AddTelegramSearch(configuration)
                .AddWolframAlpha(configuration)
                .AddFinnHubPlugin(configuration)
                .AddIexCloudPlugin(configuration)
                .AddCleverbotPlugin(configuration)
                .AddSummarizationFeature(configuration)
                .BuildServiceProvider();

            var clients = services.GetServices<IBotClient>().ToList();
            var filterExecutor = services.GetService<FilterExecutor>();
            var commandExecutor = services.GetService<CommandExecutor>();
            var systemSettings = services.GetService<SystemSettings>();

            var culture = CultureInfo.GetCultureInfo(systemSettings.Localization);
            Thread.CurrentThread.CurrentCulture = culture;
            CultureInfo.CurrentCulture = culture;

            await Task.Run(() =>
            {
                foreach (var botClient in clients)
                {
                    botClient.OnMessage += (sender, message) =>
                    {
                        Task.Run(async () =>
                        {
                            try
                            {
                                message = await filterExecutor.Execute(botClient, message);
                                if (botClient.IsCommand(message))
                                {
                                    var command = Command.FromMessage(message);
                                    await commandExecutor.Execute(botClient, command);
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                                botClient.SendTextAsync(message, message.Target, $"*Error:* {ex.Message}",
                                    ParseMessageAs.Markdown, message.Id);
                            }
                        });
                    };

                    AppDomain.CurrentDomain.UnhandledException += async (o, e) =>
                    {
                        Console.WriteLine(e.ExceptionObject.ToString());
                        var userEntity = new User { Id = botClient.BotOwnerId };
                        await botClient.SendTextAsync(new Message() { Sender = userEntity }, userEntity, e.ExceptionObject.ToString());
                    };

                    try
                    {
                        ((BotClientBase)botClient).Start();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            });

            Console.ReadLine();
        }
    }
}
