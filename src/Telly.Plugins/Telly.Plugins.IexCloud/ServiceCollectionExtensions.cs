using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;

namespace Telly.Plugins.IexCloud
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddIexCloudPlugin(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSettings<IexCloudSettings>(configuration);
            services.AddCommandHandler<StockQuoteHandler>();
            services.AddCommandHandler<ChartHandler>();

            return services;
        }
    }
}