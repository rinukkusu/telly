using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IEXSharp;
using IEXSharp.Helper;
using IEXSharp.Model;
using IEXSharp.Model.CoreData.StockPrices.Response;
using ScottPlot;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using Telly.Extensions;

namespace Telly.Plugins.IexCloud
{
    public class ChartHandler : ICommandHandler
    {
        public string CommandName => "iexchart";
        public string Description => "Renders chart for symbol.";
        public IList<IArgument> Arguments  => new List<IArgument>
        {
            new TextArgument("symbol")
        };

        public bool IsMatch(Command command) => command.Name == CommandName;
        
        private readonly DateTime _baseDate = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        private long GetUnixFromDate(DateTime date) => (long)(date - _baseDate).TotalMilliseconds;

        private readonly IEXCloudClient _client;
        
        public ChartHandler(IexCloudSettings settings)
        {
            _client =  new IEXCloudClient(
                settings.PublishableToken, 
                settings.SecretToken, 
                false, 
                false);
        }
        
        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var args = this.ParseArguments(command.Arguments);
            var symbol = args["symbol"];

            var prices = await _client.StockPrices.IntradayPricesAsync(symbol) as IEXResponse<IEnumerable<IntradayPriceResponse>>;
            if (prices?.Data == null)
                throw new Exception($"No chart returned for \"{symbol}\".");
            
            var plt = new Plot(800, 600);

            OHLC[] ohlcs = prices.Data.Select(x => x.open == null || x.high == null || x.low == null || x.close == null
                ? (OHLC)null
                : new OHLC(
                      (double) (x.open ?? 0),
                      (double) (x.high ?? 0),
                      (double) (x.low ?? 0),
                      (double) (x.close ?? 0),
                      x.GetTimestampInUTC().ToLocalTime(),
                      1D
                  )).Where(x => x != null).ToArray();
            
            plt.Title($"Intraday Data for {symbol.ToUpper()}");
            plt.YLabel("Stock Price (USD)");
            plt.PlotCandlestick(ohlcs);
            plt.Ticks(dateTimeX: true);

            var bitmap = plt.GetBitmap();

            await using var stream = new MemoryStream();
            bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
            return new DataResult(stream.ToArray(), DataType.Photo);
        }
    }
}
