using Telly.Abstractions.Attributes;

namespace Telly.Plugins.IexCloud
{
    [Setting("Plugins", "IexCloud")]
    public class IexCloudSettings
    {
        public string PublishableToken { get; set; }
        public string SecretToken { get; set; }
    }
}