using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IEXSharp;
using IEXSharp.Model;
using IEXSharp.Model.Shared.Response;
using Newtonsoft.Json;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using Telly.Extensions;

namespace Telly.Plugins.IexCloud
{
    public class StockQuoteHandler : ICommandHandler
    {
        public string CommandName => "iex";
        public string Description => "Gathers quotes from IEX Cloud";

        public IList<IArgument> Arguments => new List<IArgument>
        {
            new TextArgument("symbol")
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        private readonly IexCloudSettings _settings;
        private IEXCloudClient _client => new IEXCloudClient(
            _settings.PublishableToken, 
            _settings.SecretToken, 
            false, 
            false);

        private readonly DateTime _baseDate = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        private DateTime GetDateFromUnix(long? time) => _baseDate.AddMilliseconds((double)(time ?? 0));

        public StockQuoteHandler(IexCloudSettings settings)
        {
            _settings = settings;
        }

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var args = this.ParseArguments(command.Arguments);
            var symbol = args["symbol"];

            Quote quoteData = null;

            try 
            {
                var quote = await _client.StockPrices.QuoteAsync(symbol) as IEXResponse<Quote>;
                quoteData = quote?.Data;  
            }
            catch (Exception ex)
            {
                if (!ex.Message.StartsWith("{\"symbol")) 
                    throw;
                
                quoteData = JsonConvert.DeserializeObject<Quote>(ex.Message);
            }

            if (quoteData == null)
                throw new Exception($"No quote returned for \"{symbol}\".");

            var isMarketOpen = quoteData.latestSource != "Close";

            if (isMarketOpen)
            {
                var change = quoteData.changePercent * 100;

                var response = GetResponse(quoteData.symbol, quoteData.companyName, quoteData.latestPrice, change, "since open");
                return new TextResult(response, ParseMessageAs.Markdown);
            }
            else
            {
                var hasExtendedData = quoteData.extendedPrice != null;

                var price = hasExtendedData ? quoteData.extendedPrice : quoteData.latestPrice;
                var change = (hasExtendedData ? quoteData.extendedChangePercent : quoteData.changePercent) * 100;

                var source = hasExtendedData 
                    ? "pre/after market"
                    : "at close";

                var response = GetResponse(quoteData.symbol, quoteData.companyName, price, change, source);
                return new TextResult(response, ParseMessageAs.Markdown);
            }
        }

        private string GetResponse(string symbol, string company, decimal? price, decimal? change, string source) 
        {
            var changePrefix = change >= 0 ? "+" : "";
            var priceText = price < 1 ? $"{price:F4}" : $"{price:F2}";
            return $"{symbol}: *{priceText}* ({changePrefix}{change:F1}% {source})";
        }
    }
}
