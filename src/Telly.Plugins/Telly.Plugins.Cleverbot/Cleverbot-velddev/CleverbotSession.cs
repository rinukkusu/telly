using System;
using System.Threading.Tasks;

namespace Cleverbot
{
    public class CleverbotSession
    {

        private readonly string _apiKey;
        private string _conversationId;

        /// <summary>
        /// Creates a Cleverbot instance.
        /// </summary>
        /// <param name="apikey">Your api key obtained from https://cleverbot.com/api/ </param>
        public CleverbotSession(string apikey)
        {
            if (string.IsNullOrWhiteSpace(apikey))
            {
                throw new Exception("You can't connect without a API key.");
            }

            _apiKey = apikey;
        }

        /// <summary>
        /// Send a message to cleverbot asynchronously and get a response.
        /// </summary>
        /// <param name="message">your message sent to cleverbot</param>
        /// <returns>response from the cleverbot.com api</returns>
        public async Task<CleverbotResponse> GetResponseAsync(string message)
        {
            var response = await CleverbotResponse.CreateAsync(message, _conversationId, _apiKey);
            _conversationId = response.conversationId;
            return response;
        }
    }
}
