
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;
using Cleverbot;

namespace Telly.Plugins.Cleverbot
{
    public class TalkHandler : ICommandHandler
    {
        public string CommandName => "talk";

        public string Description => "Talk with Cleverbot";

        public bool IsMatch(Command command) => CommandName == command.Name;

        public IList<IArgument> Arguments => new List<IArgument>
        {
            new CatchAllArgument("text")
        };

        private readonly CleverbotSettings _settings;
        private readonly IDictionary<dynamic, CleverbotSession> _sessions = new Dictionary<dynamic, CleverbotSession>();

        public TalkHandler(CleverbotSettings settings)
        {
            _settings = settings;
        }

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var id = command.OriginalMessage.Target.Id;
            if (!_sessions.ContainsKey(id)) {
                _sessions.Add(id, new CleverbotSession(_settings.ApiKey));
            }

            var session = _sessions[id];
            var response = await session.GetResponseAsync(command.Arguments);

            return new TextResult(response.Response, Abstractions.Enums.ParseMessageAs.Default, true);
        }
    }
}
