using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;
using Telly.Extensions;

namespace Telly.Plugins.Cleverbot
{
    public class TalkMessageFilter : IMessageFilter
    {
        private readonly Regex _regex = new Regex(@"(^|\s)\@\S+");
        public Task<Message> Execute(IBotClient client, Message message)
        {
            if (!message.HasMentionedBot || message.Text.IsNullOrWhiteSpace() || message.Text.StartsWith(client.CommandPrefix))
                return Task.FromResult(message);

            var filteredMessage = _regex.Replace(message.Text, "");

            return Task.FromResult(new Message
            {
                Date = message.Date,
                Id = message.Id,
                Mentions = message.Mentions,
                Platform = message.Platform,
                Sender = message.Sender,
                Target = message.Target,
                Text = $"{client.CommandPrefix}talk {filteredMessage}"
            });
        }
    }
}
