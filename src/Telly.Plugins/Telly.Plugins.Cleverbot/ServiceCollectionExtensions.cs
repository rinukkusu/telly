using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;

namespace Telly.Plugins.Cleverbot
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCleverbotPlugin(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSettings<CleverbotSettings>(configuration);
            services.AddCommandHandler<TalkHandler>();
            services.AddMessageFilter<TalkMessageFilter>();

            return services;
        }
    }
}
