using Telly.Abstractions.Attributes;

namespace Telly.Plugins.Cleverbot
{
    [Setting("Plugins", "Cleverbot")]
    public class CleverbotSettings
    {
        public string ApiKey { get; set; }
    }
}
