﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Telly.Plugins.Summarize
{
    public class FixedSizedQueue<T> : ConcurrentQueue<T>
    {
        private readonly object _syncObject = new object();

        public int Size { get; }

        public FixedSizedQueue(int size = 100)
        {
            Size = size;
        }

        public FixedSizedQueue(IEnumerable<T> data, int size = 100)
        {
            Size = size;
            foreach (var x1 in data)
            {
                Enqueue(x1);
            }
        }

        public new void Enqueue(T obj)
        {
            base.Enqueue(obj);
            lock (_syncObject)
            {
                while (Count > Size)
                {
                    TryDequeue(out var outObj);
                }
            }
        }
    }
}