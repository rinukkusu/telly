﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using Telly.Extensions;

namespace Telly.Plugins.Summarize
{
    public class PoemCommandHandler : ICommandHandler
    {
        public string CommandName => "poem";
        public string Description => "Responds with a poem about the last X messages.";

        public IList<IArgument> Arguments => new List<IArgument>
        {
            new IntArgument("count", optional: true)
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        private readonly OpenAiHelperService _openAiHelperService;

        public PoemCommandHandler(OpenAiHelperService openAiHelperService)
        {
            _openAiHelperService = openAiHelperService;
        }

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            int.TryParse(command.Arguments, out var count);
            if (count == 0)
                count = 30;

            var result = await _openAiHelperService.GetGptText(
                command.OriginalMessage.Target.Id.ToString(),
                "Schreib ein Kurzgedicht über diese fiktive Konversation:",
                count);

            return new TextResult(result.ToString(), ParseMessageAs.Default, true);
        }
    }
}
