﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telly.Abstractions;

namespace Telly.Plugins.Summarize
{
    public class SummarizeService
    {
        private SummarizeSettings _settings;
        private readonly SettingsManager<SummarizeSettings> _settingsManager;

        private Dictionary<string, FixedSizedQueue<string>> _queueMap =
            new Dictionary<string, FixedSizedQueue<string>>();

        public SummarizeService(SettingsManager<SummarizeSettings> settingsManager)
        {
            _settingsManager = settingsManager;
        }

        private async Task Load()
        {
            if (_settings == null)
            {
                _settings = await _settingsManager.Load();
                _settings.QueueMap ??= new Dictionary<string, List<string>>();
                _queueMap = _settings.QueueMap.ToDictionary(k => k.Key, v => new FixedSizedQueue<string>(v.Value));
            }
        }

        private async Task Save()
        {
            _settings.QueueMap = _queueMap.ToDictionary(k => k.Key, v => v.Value.ToList());
            await _settingsManager.Save(_settings);
        }

        public async Task<IEnumerable<string>> GetLastX(string key, int count = 30)
        {
            await Load();

            if (!_queueMap.ContainsKey(key))
                throw new KeyNotFoundException();

            return _queueMap[key].TakeLast(count).ToList();
        }

        public async Task Add(string key, string text)
        {
            await Load();

            if (!_queueMap.ContainsKey(key))
                _queueMap.Add(key, new FixedSizedQueue<string>());

            _queueMap[key].Enqueue(text);

            await Save();
        }
    }
}