﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;
using Telly.Extensions;

namespace Telly.Plugins.Summarize
{
    public class SummarizationFilter : IMessageFilter
    {
        private readonly SummarizeService _summarizeService;

        public SummarizationFilter(SummarizeService summarizeService)
        {
            _summarizeService = summarizeService;
        }

        public async Task<Message> Execute(IBotClient client, Message message)
        {
            if (!message.Text.IsNullOrWhiteSpace() && !message.Text.StartsWith(client.CommandPrefix))
                await _summarizeService.Add(message.Target.Id.ToString(), $"{message.Sender.Name}: {message.Text}");

            return message;
        }
    }
}