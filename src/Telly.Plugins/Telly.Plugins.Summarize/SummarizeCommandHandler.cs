﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using Telly.Extensions;

namespace Telly.Plugins.Summarize
{
    public class SummarizeCommandHandler : ICommandHandler
    {
        public string CommandName => "summarize";
        public string Description => "Gets a summary from the last X messages.";

        public IList<IArgument> Arguments => new List<IArgument>
        {
            new IntArgument("count", optional: true)
        };

        public bool IsMatch(Command command) => command.Name == CommandName || command.Name == "summary";

        private readonly OpenAiHelperService _openAiHelperService;

        public SummarizeCommandHandler(OpenAiHelperService openAiHelperService)
        {
            _openAiHelperService = openAiHelperService;
        }

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            int.TryParse(command.Arguments, out var count);
            if (count == 0)
                count = 30;

            var result = await _openAiHelperService.GetGptText(
                command.OriginalMessage.Target.Id.ToString(),
                "Bitte diese Unterhaltung in zwei bis fünf Sätzen zusammenfassen:",
                count);

            return new TextResult(result.ToString(), ParseMessageAs.Default, true);
        }
    }
}
