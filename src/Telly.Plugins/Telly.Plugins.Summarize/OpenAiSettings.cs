﻿using Telly.Abstractions.Attributes;

namespace Telly.Plugins.Summarize
{
    [Setting("Plugins", "OpenAI")]
    public class OpenAiSettings
    {
        public string ApiKey { get; set; }
    }
}