﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;

namespace Telly.Plugins.Summarize
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSummarizationFeature(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddManagedSettings<SummarizeSettings>();
            services.AddSettings<OpenAiSettings>(configuration);
            services.AddSingleton<SummarizeService>();
            services.AddSingleton<OpenAiHelperService>();
            services.AddMessageFilter<SummarizationFilter>();
            services.AddCommandHandler<SummarizeCommandHandler>();
            services.AddCommandHandler<SatireCommandHandler>();
            services.AddCommandHandler<DiscussCommandHandler>();
            services.AddCommandHandler<PoemCommandHandler>();

            return services;
        }
    }
}