﻿using System.Collections.Generic;
using Telly.Abstractions.Attributes;

namespace Telly.Plugins.Summarize
{
    [ManagedSetting("summaries.json")]
    public class SummarizeSettings
    {
        public Dictionary<string, List<string>> QueueMap = new Dictionary<string, List<string>>();
    }
}