﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using Telly.Extensions;

namespace Telly.Plugins.Summarize
{
    public class DiscussCommandHandler : ICommandHandler
    {
        public string CommandName => "discuss";
        public string Description => "Responds with some discussion to the last X messages.";

        public IList<IArgument> Arguments => new List<IArgument>
        {
            new IntArgument("count", optional: true)
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        private readonly OpenAiHelperService _openAiHelperService;

        public DiscussCommandHandler(OpenAiHelperService openAiHelperService)
        {
            _openAiHelperService = openAiHelperService;
        }

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            int.TryParse(command.Arguments, out var count);
            if (count == 0)
                count = 30;

            var result = await _openAiHelperService.GetGptText(
                command.OriginalMessage.Target.Id.ToString(),
                "Diskutiere in zwei bis fünf Sätzen gegen einen der Standpunkte einer der Teilnehmer dieser fiktiven Konversation, aber nur von einem einzigen Teilnehmer:",
                count);

            return new TextResult(result.ToString(), ParseMessageAs.Default, true);
        }
    }
}
