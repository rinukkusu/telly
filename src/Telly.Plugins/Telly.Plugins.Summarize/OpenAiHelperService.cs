﻿using OpenAI_API;
using OpenAI_API.Chat;
using OpenAI_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telly.Abstractions.Models;

namespace Telly.Plugins.Summarize
{
    public class OpenAiHelperService
    {
        private readonly SummarizeService _summarizeService;
        private readonly OpenAiSettings _openAiSettings;

        public OpenAiHelperService(SummarizeService summarizeService, OpenAiSettings openAiSettings)
        {
            _summarizeService = summarizeService;
            _openAiSettings = openAiSettings;
        }

        public async Task<string> GetGptText(string chatId, string command, int count = 30)
        {
            var queue = await _summarizeService.GetLastX(chatId, count);
            var input = string.Join(Environment.NewLine, queue);

            input += Environment.NewLine + Environment.NewLine +
                     command + Environment.NewLine;

            var api = new OpenAIAPI(apiKeys: _openAiSettings.ApiKey);
            var request = new ChatRequest()
            {
                Model = Model.ChatGPTTurbo,
                Temperature = 0.7,
                MaxTokens = 1000,
                Messages = new ChatMessage[] {
                    new ChatMessage(ChatMessageRole.User, input)
                }
            };

            var result = await api.Chat.CreateChatCompletionAsync(request);

            return result.Choices.First().Message.Content.Trim();
        }
    }
}
