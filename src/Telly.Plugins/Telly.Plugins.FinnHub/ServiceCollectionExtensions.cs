using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;

namespace Telly.Plugins.FinnHub
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddFinnHubPlugin(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSettings<FinnHubSettings>(configuration);
            services.AddCommandHandler<FinnHubHandler>();

            return services;
        }
    }
}