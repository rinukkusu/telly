using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;
using FinnHubNet;
using Telly.Abstractions.Enums;
using Telly.Extensions;

namespace Telly.Plugins.FinnHub
{
    public class FinnHubHandler : ICommandHandler
    {
        public string CommandName => "finnhub";
        public string Description => "Gets stock prices from FinnHub";
        public IList<IArgument> Arguments => new List<IArgument>
        {
            new TextArgument("ticker")
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        private readonly FinnSettings _finnSettings;

        public FinnHubHandler(FinnHubSettings settings)
        {
            _finnSettings = new FinnSettings
            {
                ApiKey = settings.ApiKey,
                BaseURL = "https://finnhub.io/",
                Version = "/api/v1"
            };
        }

        public Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var args = this.ParseArguments(command.Arguments);
            var ticker = args["ticker"];
            var quote = Get.Quote(_finnSettings, ticker) as Quote;
            
            if (quote is null || quote.TimeStamp == 0)
                throw new Exception($"No stonks with name {ticker} found.");

            IHandlerResult result = new TextResult(
                $"{ticker}/USD: *{quote.CurrentPrice:F2}*", ParseMessageAs.Markdown);
            
            return Task.FromResult(result);
        }
    }
}