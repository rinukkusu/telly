﻿using System;
using Telly.Abstractions.Attributes;

namespace Telly.Plugins.FinnHub
{
    [Setting("Plugins", "FinnHub")]
    public class FinnHubSettings
    {
        public string ApiKey { get; set; }
    }
}