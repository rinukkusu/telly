﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;

namespace Telly.Plugins.Weather
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddWeatherPlugin(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IWeatherClient, WttrInClient>();
            services.AddCommandHandler<WeatherHandler>();

            return services;
        }
    }
}
