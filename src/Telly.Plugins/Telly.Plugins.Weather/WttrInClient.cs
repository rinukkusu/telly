﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Telly.Plugins.Weather
{
    internal class WttrInClient : IWeatherClient
    {
        private readonly HttpClient _httpClient = new HttpClient();

        public Task<string> GetWeather(string place)
        {
            return _httpClient.GetStringAsync($"https://wttr.in/{place}?format=%c%20%C%20|%20%t%20|%20%w");
        }
    }
}
