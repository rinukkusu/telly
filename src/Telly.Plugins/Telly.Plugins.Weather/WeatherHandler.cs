﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;

namespace Telly.Plugins.Weather
{
    public class WeatherHandler : ICommandHandler
    {
        public string CommandName => "weather";

        public string Description => "Gets the weather for a given location";

        public bool IsMatch(Command command) => CommandName == command.Name;

        public IList<IArgument> Arguments => new List<IArgument>
        {
            new CatchAllArgument("location")
        };

        private readonly IWeatherClient _weatherClient;

        public WeatherHandler(IWeatherClient weatherClient)
        {
            _weatherClient = weatherClient;
        }

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var result = await _weatherClient.GetWeather(command.Arguments);

            return new TextResult(result, Abstractions.Enums.ParseMessageAs.Default, true);
        }
    }
}
