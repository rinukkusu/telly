﻿using System.Threading.Tasks;

namespace Telly.Plugins.Weather
{
    public interface IWeatherClient
    {
        public Task<string> GetWeather(string place);
    }
}
