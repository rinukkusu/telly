﻿using System.Collections.Generic;
using Telly.Abstractions.Attributes;

namespace Telly.Plugins.TelegramSearch
{
    [Setting("Plugins", "TelegramSearch")]
    public class TelegramSearchSettings
    {
        public string MongoDbConnectionString { get; set; }
        public string ChatExplorerUrl { get; set; }

        public Dictionary<string, string[]> SearchSettings { get; set; }
    }
}