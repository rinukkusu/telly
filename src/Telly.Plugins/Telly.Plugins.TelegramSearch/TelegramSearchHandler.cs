﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using Telly.Extensions;
using Telly.Tools.MongoDb;
using Telly.Tools.MongoDb.Models;

namespace Telly.Plugins.TelegramSearch
{
    public class TelegramSearchHandler : ICommandHandler
    {
        public string CommandName => "tsearch";
        public string Description => "Searches for messages in an archive.";
        public IList<IArgument> Arguments => new List<IArgument>
        {
            new TextArgument("chat"),
            new CatchAllArgument("keywords")
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        private readonly TelegramSearchSettings _telegramSearchSettings;
        private readonly SearchRepository _searchRepository;
        private readonly IMongoClient _mongoClient;

        public TelegramSearchHandler(TelegramSearchSettings telegramSearchSettings, SearchRepository searchRepository, IMongoClient mongoClient)
        {
            _telegramSearchSettings = telegramSearchSettings;
            _searchRepository = searchRepository;
            _mongoClient = mongoClient;
        }

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var args = this.ParseArguments(command.Arguments);
            var chatName = (string)args["chat"];
            var keywords = (string)args["keywords"];

            _telegramSearchSettings.SearchSettings.TryGetValue(command.OriginalMessage.Target.Id.ToString(), out string[] searchSetting);
            if (searchSetting == null || !searchSetting.Contains(chatName))
                throw new Exception($"'{chatName}' can't be searched from here.");

            var messageRepository = new MessageRepository(_mongoClient, chatName);
            var messages = await messageRepository.FindMessagesAsync(keywords);
            var messageCount = messages.Count;

            if (messageCount == 0)
            {
                return new TextResult("Search yielded no results.");
            }

            var entryId = await _searchRepository.CreateSearchEntry(new SearchEntry
            {
                DateTime = DateTime.UtcNow,
                ExpiresInSeconds = 300,
                From = command.OriginalMessage.Sender.Name,
                FromId = command.OriginalMessage.Sender.Id,
                Keywords = keywords
            });

            var url = $"Found {messageCount} matches:\n{_telegramSearchSettings.ChatExplorerUrl}/{chatName}/{entryId}";
            return new TextResult(url, ParseMessageAs.Default, true);
        }
    }
}
