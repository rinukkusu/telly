﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;
using Telly.Tools.MongoDb;

namespace Telly.Plugins.TelegramSearch
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddTelegramSearch(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSettings<TelegramSearchSettings>(configuration);
            var settings = services.BuildServiceProvider().GetService<TelegramSearchSettings>();
            services.AddMongoDb(settings.MongoDbConnectionString);

            services.AddCommandHandler<TelegramSearchHandler>();

            return services;
        }
    }
}