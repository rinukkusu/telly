using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using Telly.Extensions;

namespace Telly.Plugins.System
{
    public class ImageCommandHandler : ISystemCommandHandler
    {
        public string CommandName => "image";
        public string Description => "Grabs image from URL and sends it to group";

        public IList<IArgument> Arguments => new List<IArgument>
        {
            new CatchAllArgument("url")
        };

        private readonly HttpClient _httpClient = new HttpClient();

        public bool IsMatch(Command command) => command.Name == CommandName;

        public ImageCommandHandler()
        {
            _httpClient.DefaultRequestHeaders.UserAgent.ParseAdd("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0");
        }

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var args = this.ParseArguments(command.Arguments);
            var data = await _httpClient.GetByteArrayAsync(args["url"]);

            return new DataResult(data, DataType.Photo);
        }
    }
}