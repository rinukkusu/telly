using System.Collections.Generic;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;

namespace Telly.Plugins.System
{
    public class EchoCommandHandler : ISystemCommandHandler
    {
        public string CommandName => "echo";
        public string Description => "Echoes the given text.";
        public IList<IArgument> Arguments => new List<IArgument>
        {
            new CatchAllArgument("text")
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        public Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var message = command.Arguments.Replace("\\n", "\r\n");
            return Task.FromResult((IHandlerResult) new TextResult(message, ParseMessageAs.Markdown));
        }
    }
}
