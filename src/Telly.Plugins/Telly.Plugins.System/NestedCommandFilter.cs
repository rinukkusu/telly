using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;
using Telly.Extensions;

namespace Telly.Plugins.System
{
    class NestedCommandFilter : IMessageFilter
    {
        private readonly IEnumerable<ICommandHandler> _handlers;
        private readonly Regex _nestedCommandRegex = new Regex(@"\$\((?<command>[^\)]+)\)");

        public NestedCommandFilter(IEnumerable<ISystemCommandHandler> systemHandlers, IEnumerable<ICommandHandler> handlers)
        {
            var combined = new List<ICommandHandler>(systemHandlers);
            combined.AddRange(handlers);
            _handlers = combined;
        }

        public async Task<Message> Execute(IBotClient client, Message message)
        {
            if (!client.IsCommand(message)) 
                return message;
            
            // skip evaluation for alias
            if (Command.FromMessage(message).Name == "alias")
                return message;

            message.Text = await _nestedCommandRegex.ReplaceAsync(message.Text, async (match) => await EvaluateNestedCommand(client, message, match));

            return message;
        }

        private async Task<string> EvaluateNestedCommand(IBotClient client, Message originalMessage, Match match) 
        {
            var innerMessage = new Message() {
                Platform = originalMessage.Platform,
                Id = originalMessage.Id,
                Date = originalMessage.Date,
                Sender = originalMessage.Sender,
                Target = originalMessage.Target,
                Text = match.Groups["command"].Value
            };

            if (!client.IsCommand(innerMessage)) 
                return innerMessage.Text;

            var command = Command.FromMessage(innerMessage);
            var handler = _handlers.FirstOrDefault(x => x.IsMatch(command));

            if (handler == null)
                return innerMessage.Text;

            var result = await handler.Handle(client, command) as TextResult;
            return result?.Text;
        }
    }
}
