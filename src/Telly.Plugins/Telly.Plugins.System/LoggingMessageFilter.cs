﻿using System;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;

namespace Telly.Plugins.System
{
    class LoggingMessageFilter : IMessageFilter
    {
        public Task<Message> Execute(IBotClient client, Message message)
        {
            Console.WriteLine($"[{message.Target.Name}] <{message.Sender.Name}> {message.Text}");

            return Task.FromResult(message);
        }
    }
}
