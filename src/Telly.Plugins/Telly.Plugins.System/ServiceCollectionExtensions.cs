using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;
using Telly.Plugins.System.Aliases;

namespace Telly.Plugins.System
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSystemPlugin(this IServiceCollection services)
        {
            services.AddManagedSettings<AliasSettings>();
            services.AddSingleton<IAliasService, AliasService>();
            services.AddMessageFilter<LoggingMessageFilter>();
            services.AddMessageFilter<AliasMessageFilter>();
            services.AddMessageFilter<NestedCommandFilter>();

            services.AddSystemCommandHandler<AliasAddCommandHandler>();
            services.AddSystemCommandHandler<AliasDeleteCommandHandler>();
            services.AddSystemCommandHandler<EchoCommandHandler>();
            services.AddSystemCommandHandler<ImageCommandHandler>();
            services.AddSystemCommandHandler<VideoCommandHandler>();
            services.AddSystemCommandHandler<ChatInfoCommandHandler>();

            return services;
        }

        private static IServiceCollection AddSystemCommandHandler<TCommandHandler>(this IServiceCollection services)
            where TCommandHandler : class, ISystemCommandHandler
        {
            services.AddSingleton<ISystemCommandHandler, TCommandHandler>();
            return services;
        }
    }
}