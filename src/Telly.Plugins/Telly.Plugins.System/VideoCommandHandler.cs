using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using Telly.Extensions;

namespace Telly.Plugins.System
{
    public class VideoCommandHandler : ISystemCommandHandler
    {
        public string CommandName => "video";
        public string Description => "Grabs 3s of video from URL and sends it to group";

        public IList<IArgument> Arguments => new List<IArgument>
        {
            new CatchAllArgument("url")
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var args = this.ParseArguments(command.Arguments);
            var url = args["url"];

            var ffmpegProcess = Process.Start("ffmpeg", $"-i {url} -to 00:00:03 -r 10 output.mp4");
            if (!ffmpegProcess.WaitForExit(10000))
            {
                return new TextResult("ffmpeg didn't render in time :(", ParseMessageAs.Default, true);
            }

            if (ffmpegProcess.ExitCode != 0)
            {
                return new TextResult("ffmpeg couldn't render your video :(", ParseMessageAs.Default, true);
            }

            var data = await File.ReadAllBytesAsync("output.mp4");

            return new DataResult(data, DataType.Video);
        }
    }
}