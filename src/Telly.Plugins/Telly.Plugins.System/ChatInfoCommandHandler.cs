using System.Collections.Generic;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;

namespace Telly.Plugins.System
{
    public class ChatInfoCommandHandler : ISystemCommandHandler
    {
        public string CommandName => "chatinfo";
        public string Description => "Returns info to the current chat.";

        public IList<IArgument> Arguments => new List<IArgument>
        {
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        public Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var message = @$"Platform: {command.OriginalMessage.Platform}
Sender: {command.OriginalMessage.Sender.Id} - {command.OriginalMessage.Sender.Name}
Target: {command.OriginalMessage.Target.Id} - {command.OriginalMessage.Target.Name}";
            return Task.FromResult((IHandlerResult)new TextResult(message, ParseMessageAs.Markdown));
        }
    }
}