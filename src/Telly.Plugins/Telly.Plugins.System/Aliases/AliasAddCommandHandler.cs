using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using Telly.Extensions;

namespace Telly.Plugins.System.Aliases
{
    public class AliasAddCommandHandler : ISystemCommandHandler
    {
        public string CommandName => "alias";
        public string Description => "Add new command alias";

        public IList<IArgument> Arguments => new List<IArgument>
        {
            new TextArgument("alias"),
            new CatchAllArgument("value")
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        private readonly IAliasService _service;

        public AliasAddCommandHandler(IAliasService service)
        {
            _service = service;
        }

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var arguments = this.ParseArguments(command.Arguments);
            var aliasKey = (string)arguments["alias"];
            var value = (string)arguments["value"];

            try
            {
                var alias = await _service.Get(aliasKey);

                if (alias.OwnerId == null || (alias.OwnerId != null && alias.OwnerId != command.OriginalMessage.Sender.Id.ToString()))
                    throw new Exception("You have no permissions to update this alias.");

                alias.Value = value;
                await _service.Delete(aliasKey);
                await _service.Add(aliasKey, alias);

                return new TextResult(
                    $"Successfully updated alias \"{aliasKey}\".",
                    ParseMessageAs.Default,
                    true);
            }
            catch (KeyNotFoundException ex)
            {
                var alias = new Alias
                {
                    AllowedChannels = null,
                    Value = value
                };

                await _service.Add(aliasKey, alias);

                return new TextResult(
                    $"Successfully added alias \"{aliasKey}\".",
                    ParseMessageAs.Default,
                    true);
            }
        }
    }
}