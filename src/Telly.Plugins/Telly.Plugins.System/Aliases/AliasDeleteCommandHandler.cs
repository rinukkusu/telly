using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using Telly.Extensions;

namespace Telly.Plugins.System.Aliases
{
    public class AliasDeleteCommandHandler : ISystemCommandHandler
    {
        public string CommandName => "aliasdelete";
        public string Description => "Delete an alias";

        public IList<IArgument> Arguments => new List<IArgument>()
        {
            new TextArgument("alias")
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        private readonly IAliasService _service;

        public AliasDeleteCommandHandler(IAliasService service)
        {
            _service = service;
        }

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var arguments = this.ParseArguments(command.Arguments);
            var aliasKey = arguments["alias"];

            var alias = await _service.Get(aliasKey);

            if (alias.OwnerId == null || (alias.OwnerId != null && alias.OwnerId != command.OriginalMessage.Sender.Id.ToString()))
                throw new Exception("You have no permissions to update this alias.");

            await _service.Delete(aliasKey);

            return new TextResult(
                $"Successfully deleted alias \"{alias}\".",
                ParseMessageAs.Default,
                true);
        }
    }
}