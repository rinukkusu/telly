using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;

namespace Telly.Plugins.System.Aliases
{
    public class AliasMessageFilter : IMessageFilter
    {
        private readonly IAliasService _aliasService;

        public AliasMessageFilter(IAliasService aliasService)
        {
            _aliasService = aliasService;
        }

        public async Task<Message> Execute(IBotClient client, Message message)
        {
            if (!client.IsCommand(message))
                return message;

            var originalText = message.Text;

            var resolvingAlias = true;
            while (resolvingAlias)
            {
                try
                {
                    var command = Command.FromMessage(message);
                    var alias = await _aliasService.Get(command.Name);

                    if (alias.AllowedChannels != null && alias.AllowedChannels.Count > 0 &&
                        !alias.AllowedChannels.Contains(message.Target.Id.ToString()))
                        throw new Exception("Restricted!");

                    var value = alias.Value;
                    value = value.StartsWith("/")
                        ? client.CommandPrefix + value.Remove(0, 1)
                        : value;
                    message.Text = $"{value} {command.Arguments}";
                }
                catch (KeyNotFoundException ex)
                {
                    // everything is good :)
                    resolvingAlias = false;
                }
                catch (Exception ex)
                {
                    message.Text = originalText;
                    break;
                }
            }

            return message;
        }
    }
}