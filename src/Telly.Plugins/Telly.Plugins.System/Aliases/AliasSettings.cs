using System.Collections.Generic;
using Telly.Abstractions.Attributes;

namespace Telly.Plugins.System.Aliases
{
    [ManagedSetting("aliases.json")]
    internal class AliasSettings
    {
        public IDictionary<string, Alias> Aliases { get; set; } = new Dictionary<string, Alias>();
    }

    public class Alias
    {
        public string Value { get; set; }
        public string OwnerId { get; set; }
        public List<string> AllowedChannels { get; set; }
    }
}