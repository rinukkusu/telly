using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Telly.Abstractions;

namespace Telly.Plugins.System.Aliases
{
    public interface IAliasService
    {
        Task<Alias> Get(string key);

        Task Add(string key, Alias alias);

        Task Delete(string key);

        IReadOnlyDictionary<string, Alias> GetAll();
    }

    internal class AliasService : IAliasService
    {
        private AliasSettings _settings;
        private readonly SettingsManager<AliasSettings> _settingsManager;

        public AliasService(SettingsManager<AliasSettings> settingsManager)
        {
            _settingsManager = settingsManager;
        }

        private async Task Load()
        {
            _settings ??= await _settingsManager.Load();
        }

        public async Task<Alias> Get(string key)
        {
            await Load();

            if (!_settings.Aliases.ContainsKey(key))
                throw new KeyNotFoundException();

            return _settings.Aliases[key];
        }

        public async Task Add(string key, Alias alias)
        {
            await Load();

            _settings.Aliases.Add(key, alias);
            await _settingsManager.Save(_settings);
        }

        public async Task Delete(string key)
        {
            if (!_settings.Aliases.ContainsKey(key))
                throw new KeyNotFoundException();

            _settings.Aliases.Remove(key);
            await _settingsManager.Save(_settings);
        }

        public IReadOnlyDictionary<string, Alias> GetAll()
        {
            return new ReadOnlyDictionary<string, Alias>(_settings.Aliases);
        }
    }
}