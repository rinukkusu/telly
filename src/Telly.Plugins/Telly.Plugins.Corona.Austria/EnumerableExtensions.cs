﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Telly.Plugins.Corona.Austria
{
    public static class EnumerableExtensions
    {
        public static TValue GetPropertyValue<T, TValue>(this T target, Expression<Func<T, TValue>> memberLamda)
        {
            var memberSelectorExpression = memberLamda.Body as MemberExpression;
            if (memberSelectorExpression != null)
            {
                var property = memberSelectorExpression.Member as PropertyInfo;
                if (property != null)
                {
                    return (TValue)property.GetValue(target);
                }
            }

            throw new Exception("Can't get property value");
        }

        public static void SetPropertyValue<T, TValue>(this T target, Expression<Func<T, TValue>> memberLamda, TValue value)
        {
            var memberSelectorExpression = memberLamda.Body as MemberExpression;
            if (memberSelectorExpression != null)
            {
                var property = memberSelectorExpression.Member as PropertyInfo;
                if (property != null)
                {
                    property.SetValue(target, value, null);
                }
            }
        }

        public static IEnumerable<T> ConvertFieldToDelta<T>(this IList<T> list, Expression<Func<T, int>> memberLambda)
            where T : ICloneable
        {
            int before = 0;
            foreach (var obj in list)
            {
                var clone = (T)obj.Clone();
                int current = clone.GetPropertyValue(memberLambda);
                clone.SetPropertyValue(memberLambda, current - before);
                before = current;

                yield return clone;
            }
        }

        public static IEnumerable<T> ConvertFieldToSum<T>(this IList<T> list, Expression<Func<T, int>> memberLambda)
            where T : ICloneable
        {
            int sum = 0;
            foreach (var obj in list)
            {
                var clone = (T)obj.Clone();
                int current = clone.GetPropertyValue(memberLambda);
                clone.SetPropertyValue(memberLambda, sum += current);

                yield return clone;
            }
        }
    }
}