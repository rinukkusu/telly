using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Telly.Plugins.Corona.Austria
{
    public class CoronaAmpelService
    {
        private const string Url =
            "https://corona-ampel.gv.at/sites/corona-ampel.gv.at/files/assets/Warnstufen_Corona_Ampel_aktuell.json";

        private readonly HttpClient _client = new HttpClient();
        private CoronaAmpelData _data;
        private DateTime? _lastUpdate;

        private async Task RefreshData()
        {
            if (_lastUpdate != null && DateTime.UtcNow - _lastUpdate < TimeSpan.FromMinutes(10))
                return;

            var json = await _client.GetStringAsync(Url);
            _data = JsonSerializer.Deserialize<CoronaAmpelData[]>(json)[0];

            _lastUpdate = DateTime.UtcNow;
        }

        public async Task<WarningEntry> GetWarningEntry(string filter)
        {
            await RefreshData();

            return _data.Warnstufen.FirstOrDefault(x => x.Name.Contains(filter, StringComparison.InvariantCultureIgnoreCase));
        }

        public async Task<WarningEntry[]> GetAll()
        {
            await RefreshData();

            return _data.Warnstufen.ToArray();
        }
    }


    public class CoronaAmpelData
    {
        [JsonPropertyName("Stand")]
        public DateTime Stand { get; set; }
        [JsonPropertyName("Warnstufen")]
        public WarningEntry[] Warnstufen { get; set; }
    }

    public class WarningEntry
    {
        private static readonly IDictionary<string, string> WarnEmojiMap = new Dictionary<string, string>
        {
            { "1", "🟢" },
            { "2", "🟡" },
            { "3", "🟠" },
            { "4", "🔴" },
        };

        [JsonPropertyName("Region")]
        public string Region { get; set; }
        [JsonPropertyName("GKZ")]
        public string Gkz { get; set; }
        [JsonPropertyName("Name")]
        public string Name { get; set; }
        [JsonPropertyName("Warnstufe")]
        public string Warnstufe { get; set; }

        public string WarnstufeEmoji => WarnEmojiMap[Warnstufe];
    }

}