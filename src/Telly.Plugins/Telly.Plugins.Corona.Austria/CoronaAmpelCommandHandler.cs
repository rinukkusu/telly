using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;

namespace Telly.Plugins.Corona.Austria
{
    public class CoronaAmpelCommandHandler : ICommandHandler
    {
        public string CommandName => "coronampel";
        public string Description => "Scraps data from corona-ampel.gv.at";
        public IList<IArgument> Arguments => new List<IArgument>
        {
            new CatchAllArgument("filter", true)
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        private readonly CoronaAmpelService _ampelService = new CoronaAmpelService();

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var entries = (await _ampelService.GetAll())
                .Where(x => x.Name != "Österreich")
                .OrderBy(x => x.Gkz)
                .ToList();

            var ampelState = "";
            var lastStateGkz = "";
            var ampel = "*Corona-Ampel Österreich*\n```\n";
            foreach (var entry in entries)
            {
                if (entry.Gkz.Length == 1 && lastStateGkz != entry.Gkz) 
                {
                    ampel += ampelState;
                    ampelState = "";
                    lastStateGkz = entry.Gkz;
                }

                var ampelLine = "";
                if (entry.Gkz.Length > 1) 
                {
                    if (entry.Warnstufe == "1")
                        continue;

                    ampelLine += "   ";
                }

                ampelLine += $"{entry.WarnstufeEmoji} {entry.Name}\n";
                ampelState += ampelLine;
            }

            if (ampelState.Length > 0)
                ampel += ampelState;

            ampel += "```";

            return new TextResult(ampel, ParseMessageAs.Markdown);
        }
    }
}