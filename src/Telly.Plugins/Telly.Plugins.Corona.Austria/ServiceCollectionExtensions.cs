﻿using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;

namespace Telly.Plugins.Corona.Austria
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCoronaAustriaPlugin(this IServiceCollection services)
        {
            services.AddCommandHandler<CoronaCommandHandler>();
            services.AddCommandHandler<CoronaAmpelCommandHandler>();
            services.AddCommandHandler<CoronaPlotCommandHandler>();

            return services;
        }
    }
}