using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;
using Telly.Plugins.Corona.Austria.Models;
using ScottPlot;

namespace Telly.Plugins.Corona.Austria
{
    public class CoronaPlotCommandHandler : ICommandHandler
    {
        public string CommandName => "coronaplot";
        public string Description => "Displays plots of COVID-19 case development for austria.";

        public IList<IArgument> Arguments => new List<IArgument>
        {
            new TextArgument("full")
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        private readonly HttpClient _httpClient = new HttpClient();

        private DateTime? _lastRefresh = null;
        private Bitmap _bitmap = null;

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            if (!_lastRefresh.HasValue || (DateTime.UtcNow - _lastRefresh.Value) > TimeSpan.FromMinutes(30))
            {
                await GeneratePlotAndGetStream(command.Arguments == "full");
                _lastRefresh = DateTime.UtcNow;
            }

            var bytes = ImageToByte2(_bitmap);

            return new DataResult(bytes, Abstractions.Enums.DataType.Photo);
        }

        public static byte[] ImageToByte2(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        private static Regex _trendRegex = new Regex(@"var dpTrend = (?<json>[^;]+)");

        public async Task GeneratePlotAndGetStream(bool full)
        {
            var url = "https://covid19-dashboard.ages.at/data/data.zip";

            var zippedCsvReader = new ZippedCsvReader(url);

            var data = await zippedCsvReader.ReadAllLines<CovidFaelleDelta>("CovidFaelleDelta.csv");
            var icuData = await zippedCsvReader.ReadAllLines<Hospitalisierung>("Hospitalisierung.csv");
            icuData = icuData.Where(x => x.BundeslandID == 10); // 10 -> Österreich

            var plt = new Plot(800, 400);
            plt.Title($"Tägliche Neuerkrankungen");

            if (full)
            {
                plt.AddHorizontalSpan(DateTime.Parse("2020-03-13").ToOADate(), DateTime.Parse("2020-04-30").ToOADate(), label: "1. Lockdown");
                plt.AddHorizontalSpan(DateTime.Parse("2020-11-17").ToOADate(), DateTime.Parse("2020-12-06").ToOADate(), label: "2. Lockdown");
                plt.AddHorizontalSpan(DateTime.Parse("2020-12-26").ToOADate(), DateTime.Parse("2021-02-07").ToOADate(), label: "3. Lockdown");
                plt.AddHorizontalSpan(DateTime.Parse("2021-03-27").ToOADate(), DateTime.Parse("2021-05-02").ToOADate(), label: "Osterferien + Lockdown Osten");
            }
            else
            {
                data = data.Where(x => x.LastUpdate > new DateTime(2021, 07, 01)).ToList();
                icuData = icuData.Where(x => x.Meldedatum > new DateTime(2021, 07, 01)).ToList();
            }

            plt.AddHorizontalSpan(DateTime.Parse("2021-11-22").ToOADate(), DateTime.Parse("2021-12-12").ToOADate(), label: "4. Lockdown");

            var xs = data.Select(x => (double)x.LastUpdate.ToOADate()).ToArray();
            var ys = data.Select(x => (double)x.NewCases).ToArray();
            plt.AddScatter(xs, ys, label: "Tägliche Neuerkrankungen", markerShape: MarkerShape.none);

            // draw average
            var avgX = new List<double>();
            var avgY = new List<double>();
            var avgFrame = 7;
            for (var i = 0; i < xs.Count(); i += avgFrame)
            {
                var bundle = xs.Skip(i).Take(avgFrame);
                if (!bundle.Any())
                    break;

                avgX.Add(bundle.Average());
                avgY.Add(ys[i]);
            }
            plt.AddScatter(avgX.ToArray(), avgY.ToArray(), label: "Wochenschnitt", markerShape: MarkerShape.none);

            // add icu
            var icuX = icuData.Select(x => (double)x.Meldedatum.ToOADate()).ToArray();
            var icuY = icuData.Select(x => (double)x.IntensivBettenBelCovid19).ToArray();
            var normalY = icuData.Select(x => (double)x.IntensivBettenBelNichtCovid19).ToArray();

            var icuScatter = plt.AddScatter(icuX, icuY, label: "Belegte Intensivbetten (Covid-19)", markerShape: MarkerShape.none, lineStyle: LineStyle.DashDot);
            var normalScatter = plt.AddScatter(icuX, normalY, label: "Belegte Normalbetten (Covid-19)", markerShape: MarkerShape.none, lineStyle: LineStyle.DashDot);
            var icuYAxis = plt.AddAxis(ScottPlot.Renderable.Edge.Left, axisIndex: 10, title: "Bettenbelegung");
            icuScatter.YAxisIndex = 10;
            normalScatter.YAxisIndex = 10;

            // add deceased
            var deadY = data.Select(x => (double)x.DeceasedCases).ToArray();
            var deadScatter = plt.AddScatter(xs, deadY, label: "Verstorben (Covid-19)", markerShape: MarkerShape.none);
            var deadYAxis = plt.AddAxis(ScottPlot.Renderable.Edge.Left, axisIndex: 20, title: "Verstorben");
            deadScatter.YAxisIndex = 20;

            plt.XAxis.DateTimeFormat(true);
            plt.Legend(location: Alignment.UpperLeft);

            if (_bitmap != null)
                _bitmap.Dispose();
            _bitmap = plt.GetBitmap();

            /*if (_bitmap != null)
                _bitmap.Dispose();

            _bitmap = new Bitmap(800, 1200);

            using (plot)
            using (plotCured)
            using (plotDeceased)
            //using (plotCurrent)
            using (var canvas = Graphics.FromImage(_bitmap))
            {
                canvas.DrawImage(plot, new Point(0, 0));
                canvas.DrawImage(plotCured, new Point(0, 400));
                canvas.DrawImage(plotDeceased, new Point(0, 800));
                //canvas.DrawImage(plotCurrent, new Point(0, 1200));
                canvas.Save();
            }*/
        }

        private class TrendEntry
        {
            public string Label { get; set; }
            public int Y { get; set; }
        }
    }
}