using System;
using CsvHelper.Configuration.Attributes;

namespace Telly.Plugins.Corona.Austria.Models
{
    public class NormalBedUsage
    {
        [Name("time")]
        public DateTime Date { get; set; }

        [Name("Belegung Normalbetten in %")]
        public double Usage { get; set; }
    }

    public class IcuBedUsage
    {
        [Name("time")]
        public DateTime Date { get; set; }

        [Name("Belegung Intensivbetten in %")]
        public double Usage { get; set; }
    }
}
