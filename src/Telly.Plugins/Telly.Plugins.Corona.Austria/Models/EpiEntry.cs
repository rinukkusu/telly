using System;
using CsvHelper.Configuration.Attributes;

namespace Telly.Plugins.Corona.Austria.Models
{
    public class EpiEntry : ICloneable
    {
        [Name("time")]
        public DateTime Date { get; set; }

        [Name("tägliche Erkrankungen")]
        public int DailyCases { get; set; }

        public object Clone() => MemberwiseClone();
    }


    public class CuredEntry : ICloneable
    {
        [Name("time")]
        public DateTime Date { get; set; }

        [Name("Genesen")]
        public int DailyCured { get; set; }

        public object Clone() => MemberwiseClone();
    }

    public class DeceasedEntry : ICloneable
    {
        [Name("time")]
        public DateTime Date { get; set; }

        [Name("Todesfälle")]
        public int DailyDeceased { get; set; }

        public object Clone() => MemberwiseClone();
    }
}
