using System;
using CsvHelper.Configuration.Attributes;

namespace Telly.Plugins.Corona.Austria.Models
{
    public class AllgemeinDaten
    {
        [Name("AktuelleErkrankungen")]
        public int ActiveCases { get; set; }

        [Name("PositivGetestet")]
        public int TotalCases { get; set; }

        [Name("Genesen")]
        public int CuredCases { get; set; }

        [Name("TotBestaetigt")]
        public int DeceasedCases { get; set; }

        [Name("GesTestungen")]
        public int TotalTests { get; set; }

        [Name("GesNBVerf")]
        public int TotalBedsNormal { get; set; }

        [Name("GesIBVerf")]
        public int TotalBedsIntensiveCare { get; set; }

        [Name("GesNBBel")]
        public int BedsNormalInUse { get; set; }

        [Name("GesIBBel")]
        public int BedsIntensiveCareInUse { get; set; }

        [Name("BFNH")]
        public int NotHospitalized { get; set; }

        [Name("LetzteAktualisierung")]
        [Format("dd.MM.yyyy HH:mm:ss")]
        public DateTime LastUpdate { get; set; }
    }
}
