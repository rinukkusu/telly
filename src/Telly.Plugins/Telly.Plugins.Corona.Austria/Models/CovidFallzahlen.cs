using System;
using CsvHelper.Configuration.Attributes;

namespace Telly.Plugins.Corona.Austria.Models
{
    public class CovidFallzahlen
    {
        [Name("MeldeDatum")]
        [Format("dd.MM.yyyy HH:mm:ss")]
        public DateTime LastUpdate { get; set; }
        
        [Name("TestGesamt")]
        public int TotalTests { get; set; }
        
        [Name("FZHospFree")]
        public int FreeBedsNormal { get; set; }

        [Name("FZICUFree")]
        public int FreeBedsIntensiveCare { get; set; }

        [Name("FZHosp")]
        public int BedsNormalInUse { get; set; }

        [Name("FZICU")]
        public int BedsIntensiveCareInUse { get; set; }
    }
}