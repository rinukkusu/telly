using System;
using CsvHelper.Configuration.Attributes;

namespace Telly.Plugins.Corona.Austria.Models
{
    public class CovidFaelleDelta
    {
        [Name("Datum")]
        [Format("dd.MM.yyyy HH:mm:ss")]
        public DateTime LastUpdate { get; set; }
        
        [Name("DeltaAnzahlVortag")]
        public int NewCases { get; set; }
        
        [Name("DeltaGeheiltVortag")]
        public int CuredCases { get; set; }
        
        [Name("DeltaTotVortag")]
        public int DeceasedCases { get; set; }
        
        [Name("DeltaAktivVortag")]
        public int ActiveCases { get; set; }
    }
}
