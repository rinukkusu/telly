﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration.Attributes;

namespace Telly.Plugins.Corona.Austria.Models
{
    public class Hospitalisierung
    {
        [Name("Meldedatum")]
        [Format("dd.MM.yyyy HH:mm:ss")]
        public DateTime Meldedatum { get; set; }

        [Name("BundeslandID")]
        public int BundeslandID { get; set; }

        [Name("Bundesland")]
        public string Bundesland { get; set; }

        [Name("IntensivBettenBelCovid19")]
        public int IntensivBettenBelCovid19 { get; set; }

        [Name("IntensivBettenBelNichtCovid19")]
        public int IntensivBettenBelNichtCovid19 { get; set; }

        [Name("IntensivBettenKapGes")]
        public int IntensivBettenKapGes { get; set; }
    }
}