using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using Telly.Plugins.Corona.Austria.Models;

namespace Telly.Plugins.Corona.Austria
{
    public class CoronaCommandHandler : ICommandHandler
    {
        public string CommandName => "corona";
        public string Description => "Retrieves COVID-19 case information for austria.";
        public IList<IArgument> Arguments => new List<IArgument>();

        public bool IsMatch(Command command) => command.Name == CommandName;

        private readonly CoronaAmpelService _coronaAmpelService = new CoronaAmpelService();

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var url = "https://covid19-dashboard.ages.at/data/data.zip";

            var zippedCsvReader = new ZippedCsvReader(url);
            var data = await zippedCsvReader.ReadLastLine<CovidFallzahlen>("CovidFallzahlen.csv");
            var covidFaelleDelta = (await zippedCsvReader.ReadAllLines<CovidFaelleDelta>("CovidFaelleDelta.csv")).ToList();
            var dailyDelta = covidFaelleDelta.Last();
            
            var totalCases = covidFaelleDelta.Sum(x => x.NewCases);
            var currentActive = covidFaelleDelta.Sum(x => x.ActiveCases);
            var totalDeceased = covidFaelleDelta.Sum(x => x.DeceasedCases);

            var warnData = await _coronaAmpelService.GetWarningEntry("Graz");

            var nbUsage = data.BedsNormalInUse / (double) (data.FreeBedsNormal + data.BedsNormalInUse)*100;
            var icuUsage = data.BedsIntensiveCareInUse / (double) (data.FreeBedsIntensiveCare + data.BedsIntensiveCareInUse)*100;

            var str = $@"Stand {data.LastUpdate}
Tests: *{data.TotalTests:n0}*
Fälle gesamt: *{totalCases:n0}*
  aktuell krank: *{currentActive:n0}*
  neue Fälle: *{dailyDelta.NewCases:n0}* ({(dailyDelta.LastUpdate == DateTime.Today ? "heute" : dailyDelta.LastUpdate.ToShortDateString())})
Hospitalisiert: *{(data.BedsNormalInUse + data.BedsIntensiveCareInUse):n0}* ({nbUsage:f1}% belegt)
  davon ICU: *{data.BedsIntensiveCareInUse:n0}* ({icuUsage:f1}% belegt)
Todesfälle: *{totalDeceased:n0}*
Corona-Ampel {warnData.Name}: {warnData.WarnstufeEmoji}
";

            return new TextResult(str, ParseMessageAs.Markdown);
        }
    }
}
