using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;

namespace Telly.Plugins.Corona.Austria
{
    public class ZippedCsvReader : IDisposable
    {
        private readonly HttpClient _httpClient = new HttpClient();
        private readonly string _url;
        private Stream _zipDataStream;
        private ZipArchive _zipArchive;

        public ZippedCsvReader(string url)
        {
            _url = url;
        }

        public ZippedCsvReader(Stream stream)
        {
            _zipDataStream = stream;
        }

        private async Task LoadArchive()
        {
            if (_zipArchive != null)
                return;

            if (_url != null)
                _zipDataStream = await _httpClient.GetStreamAsync(_url);

            _zipArchive = new ZipArchive(_zipDataStream, ZipArchiveMode.Read);
        }

        private ZipArchiveEntry GetEntry(string zippedFilename)
        {
            var entry = _zipArchive.GetEntry(zippedFilename);
            if (entry == null)
                throw new FileNotFoundException(
                    $"File \"{zippedFilename}\" could not be found in Zip archive.",
                    zippedFilename);

            return entry;
        }

        private async Task<T> Read<T>(string zippedFilename, Func<CsvReader, T> readDelegate, string csvDelimiter = ";")
        {
            await LoadArchive();

            var entry = GetEntry(zippedFilename);

            using var streamReader = new StreamReader(entry.Open());
            using var csvReader = new CsvReader(streamReader, new CsvConfiguration(CultureInfo.GetCultureInfo("de-AT"))
            {
                Delimiter = csvDelimiter
            });

            return readDelegate(csvReader);
        }

        public Task<T> ReadFirstLine<T>(string zippedFilename, string csvDelimiter = ";")
        {
            return Read(zippedFilename, reader =>
            {
                reader.Read(); // set head to first line
                reader.ReadHeader(); // read header
                reader.Read(); // set head to second line (actual data)
                return reader.GetRecord<T>();
            }, csvDelimiter);
        }

        public Task<T> ReadLastLine<T>(string zippedFilename, string csvDelimiter = ";")
            where T : new()
        {
            return Read(zippedFilename, reader =>
            {
                reader.Read(); // set head to first line
                reader.ReadHeader(); // read header
                var record = new T();
                reader.EnumerateRecords(record).LastOrDefault();
                return record;
            }, csvDelimiter);
        }

        public async Task<IEnumerable<T>> ReadAllLines<T>(string zippedFilename, string csvDelimiter = ";")
        {
            return (
                await Read(
                    zippedFilename,
                    reader => reader.GetRecords<T>().ToList(),
                    csvDelimiter
                )
            ).ToList();
        }

        public void Dispose()
        {
            _httpClient?.Dispose();
            _zipArchive?.Dispose();
            _zipDataStream?.Dispose();
        }
    }
}