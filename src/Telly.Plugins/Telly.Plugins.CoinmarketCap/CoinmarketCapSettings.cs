﻿using Telly.Abstractions.Attributes;

namespace Telly.Plugins.CoinmarketCap
{
    [Setting("Plugins", "CoinmarketCap")]
    public class CoinmarketCapSettings
    {
        public string ApiKey { get; set; }
    }
}
