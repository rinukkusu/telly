﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;

namespace Telly.Plugins.CoinmarketCap
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCoinmarketCapPlugin(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSettings<CoinmarketCapSettings>(configuration);
            services.AddCommandHandler<CoinmarketCapHandler>();

            return services;
        }
    }
}
