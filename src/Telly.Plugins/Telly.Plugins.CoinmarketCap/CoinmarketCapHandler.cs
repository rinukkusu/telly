﻿using System;
using System.Collections.Generic;
using NoobsMuc.Coinmarketcap.Client;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;
using Telly.Extensions;
using Telly.Abstractions.Enums;

namespace Telly.Plugins.CoinmarketCap
{
    public class CoinmarketCapHandler : ICommandHandler
    {
        public string CommandName => "cmc";
        public string Description => "Retrieves CoinmarketCap stuff.";

        public bool IsMatch(Command command) => command.Name == CommandName;

        protected readonly ICoinmarketcapClient _coinmarketcapClient;

        public IList<IArgument> Arguments => new List<IArgument>
        {
            new TextArgument("coinName")
        };

        public CoinmarketCapHandler(CoinmarketCapSettings settings)
        {
            _coinmarketcapClient = new CoinmarketcapClient(settings.ApiKey);
        }

        public Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var args = this.ParseArguments(command.Arguments);
            var slug = (string)args["coinName"];

            try
            {
                var currency = _coinmarketcapClient.GetCurrencyBySlug(slug);

                IHandlerResult result = new TextResult(
                    $"{currency.Name}/{currency.ConvertCurrency}: *{currency.Price:F5}* ({(currency.PercentChange24h > 0 ? "+" : "")}{currency.PercentChange24h:F1}%)",
                    ParseMessageAs.Markdown);

                return Task.FromResult(result);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    throw ex.InnerException?.InnerException ?? ex.InnerException;

                throw;
            }
        }
    }
}
