using Telly.Abstractions.Attributes;

namespace Telly.Plugins.WolframAlpha
{
    [Setting("Plugins", "WolframAlpha")]
    public class WolframAlphaSettings
    {
        public string AppId { get; set; }
    }
}
