﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Genbox.WolframAlpha;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;

namespace Telly.Plugins.WolframAlpha
{
    public class WolframAlphaHandler : ICommandHandler
    {
        public string CommandName => "wolframalpha";
        public string Description => "Sends calculations to WolframAlpha";
        public IList<IArgument> Arguments => new List<IArgument>
        {
            new CatchAllArgument("data")
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        private readonly WolframAlphaClient _wolframAlphaClient;

        public WolframAlphaHandler(WolframAlphaSettings settings)
        {
            _wolframAlphaClient = new WolframAlphaClient(settings.AppId);
        }

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var result = await _wolframAlphaClient.ShortAnswerAsync(command.Arguments);
            return new TextResult(result, ParseMessageAs.Default, true);
        }
    }
}