using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;

namespace Telly.Plugins.WolframAlpha
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddWolframAlpha(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSettings<WolframAlphaSettings>(configuration);
            services.AddCommandHandler<WolframAlphaHandler>();

            return services;
        }
    }
}