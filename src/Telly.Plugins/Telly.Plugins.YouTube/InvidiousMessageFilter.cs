﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;

namespace Telly.Plugins.YouTube
{
    public class InvidiousMessageFilter : IMessageFilter
    {
        private static readonly Regex YoutubeRegex = new Regex(@"(youtube.com\/watch\?v=|youtu.be\/)(?<id>[A-Za-z0-9\-_]+)");
        internal const string InvidiousUrlBase = "https://invidious.snopyta.org/watch?v=";

        public async Task<Message> Execute(IBotClient client, Message message)
        {
            if (message.Text == null || client.IsCommand(message) || !YoutubeRegex.IsMatch(message.Text)) 
                return message;

            var videoId = YoutubeRegex.Match(message.Text).Groups["id"].Value;

            await client.SendTextAsync(
                message,
                message.Target,
                $"{InvidiousUrlBase}{videoId}",
                ParseMessageAs.Default,
                message.Id,
                true);

            return message;
        }
    }
}