using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;

namespace Telly.Plugins.YouTube
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddYouTubePlugin(this IServiceCollection services, IConfiguration config)
        {
            services.AddSettings<YouTubeSettings>(config);
            services.AddCommandHandler<YouTubeCommandHandler>();
            //services.AddMessageFilter<InvidiousMessageFilter>();

            return services;
        }
    }
}