using Telly.Abstractions.Attributes;

namespace Telly.Plugins.YouTube
{
    [Setting("Plugins", "YouTube")]
    public class YouTubeSettings
    {
        public string ApplicationName { get; set; }
        public string ApiKey { get; set; }
    }
}