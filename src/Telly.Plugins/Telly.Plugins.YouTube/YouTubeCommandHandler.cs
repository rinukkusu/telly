﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Telly.Abstractions;
using Telly.Abstractions.Models;

namespace Telly.Plugins.YouTube
{
    public class YouTubeCommandHandler : ICommandHandler
    {
        public string CommandName => "youtube";
        public string Description => "Search for videos on YouTube";

        public IList<IArgument> Arguments => new List<IArgument>()
        {
            new CatchAllArgument("searchText")
        };

        public bool IsMatch(Command command) => command.Name == CommandName;

        private readonly YouTubeService _service;

        public YouTubeCommandHandler(YouTubeSettings settings)
        {
            _service = new YouTubeService(new BaseClientService.Initializer
            {
                ApplicationName = settings.ApplicationName,
                ApiKey = settings.ApiKey
            });
        }

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var listRequest = _service.Search.List("snippet");
            listRequest.Q = command.Arguments;
            listRequest.MaxResults = 1;
            listRequest.Type = "video";

            var results = await listRequest.ExecuteAsync();
            var firstVideoId = results.Items.First().Id.VideoId;

            return new TextResult($@"https://youtu.be/{firstVideoId}");
        }
    }
}