﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;

namespace Telly.Plugins.Booru
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddBooruPlugin(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSettings<BooruSettings>(configuration);
            services.AddCommandHandler<GelbooruCommandHandler>();
            services.AddCommandHandler<Rule34BooruCommandHandler>();
            services.AddCommandHandler<BooruMoreCommandHandler>();
            services.AddCommandHandler<LoliBooruCommandHandler>();

            return services;
        }
    }
}
