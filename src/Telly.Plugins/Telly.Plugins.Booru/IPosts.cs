﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Telly.Plugins.Booru
{
    public interface IPosts<T> where T : IPost
    {
        T[] Post { get; set; }
    }

    public interface IPost
    {
        public string File_url { get; set; }
        public string Sample_url { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}