﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Telly.Abstractions;
using Telly.Abstractions.Models;

namespace Telly.Plugins.Booru
{
    public abstract class BooruCommandHandlerBase<T, Z> : ICommandHandler
        where T : IPosts<Z>
        where Z : IPost
    {
        public abstract string CommandName { get; }
        public abstract string Description { get; }
        protected abstract string Url { get; }

        private readonly HttpClient _client = new HttpClient();

        public IList<IArgument> Arguments => new List<IArgument>
        {
            new CatchAllArgument("keywords")
        };

        public async Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            var args = string.Join("+", command.Arguments.Split(" "));

            var xmlStream = await _client.GetStreamAsync($"{Url}&tags={args}");
            var serializer = new XmlSerializer(typeof(T));
            var result = ((T)serializer.Deserialize(xmlStream));
            var posts = result.Post.Where(x =>
                x.File_url.EndsWith("jpeg") ||
                x.File_url.EndsWith("jpg") ||
                x.File_url.EndsWith("png"))
                .Cast<IPost>()
                .ToList();

            BooruMoreCommandHandler.SetHistory(command.OriginalMessage.Sender.Id, posts);

            var firstFive = posts
                .Take(5)
                .Select(x => x.Height > 1500 || x.Width > 1500 ? x.Sample_url : x.File_url)
                .ToArray();

            if (!firstFive.Any())
                throw new Exception("Nothing found.");

            return new DataCollectionsFromUrlsResult(firstFive, Abstractions.Enums.DataType.Photo, null, true);
        }

        public abstract bool IsMatch(Command command);
    }
}