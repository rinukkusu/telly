﻿using Telly.Abstractions.Models;

namespace Telly.Plugins.Booru
{
    internal class LoliBooruCommandHandler : BooruCommandHandlerBase<Posts, Post>
    {
        public override string CommandName => "lolibooru";
        public override string Description => ";)";

        protected override string Url => "https://lolibooru.moe/post/index.xml?dummy";

        public override bool IsMatch(Command command) => command.Name == CommandName || command.Name == "lb";
    }
}