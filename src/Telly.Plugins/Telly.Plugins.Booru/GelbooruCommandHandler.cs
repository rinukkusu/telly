﻿using Telly.Abstractions.Models;

namespace Telly.Plugins.Booru
{
    internal class GelbooruCommandHandler : BooruCommandHandlerBase<Posts_Nodes, Post_Nodes>
    {
        public override string CommandName => "gelbooru";
        public override string Description => ";)";

        protected override string Url => $"https://gelbooru.com/index.php?page=dapi&s=post&q=index&api_key={_booruSettings.GelbooruApiKey}&user_id={_booruSettings.GelbooruUserId}";

        private readonly BooruSettings _booruSettings;

        public GelbooruCommandHandler(BooruSettings booruSettings)
        {
            _booruSettings = booruSettings;
        }

        public override bool IsMatch(Command command) => command.Name == CommandName || command.Name == "gb";
    }
}