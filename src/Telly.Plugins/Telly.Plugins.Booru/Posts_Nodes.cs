﻿namespace Telly.Plugins.Booru
{
    // HINWEIS: Für den generierten Code ist möglicherweise mindestens .NET Framework 4.5 oder .NET Core/Standard 2.0 erforderlich.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Posts_Nodes : IPosts<Post_Nodes>
    {
        private Post_Nodes[] postField;

        private byte limitField;

        private byte offsetField;

        private ushort countField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("post")]
        public Post_Nodes[] Post
        {
            get
            {
                return this.postField;
            }
            set
            {
                this.postField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte limit
        {
            get
            {
                return this.limitField;
            }
            set
            {
                this.limitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte offset
        {
            get
            {
                return this.offsetField;
            }
            set
            {
                this.offsetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort count
        {
            get
            {
                return this.countField;
            }
            set
            {
                this.countField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class Post_Nodes : IPost
    {
        private uint idField;

        private string created_atField;

        private byte scoreField;

        private int widthField;

        private int heightField;

        private string md5Field;

        private string directoryField;

        private string imageField;

        private string ratingField;

        private string sourceField;

        private uint changeField;

        private string ownerField;

        private uint creator_idField;

        private byte parent_idField;

        private byte sampleField;

        private byte preview_heightField;

        private byte preview_widthField;

        private string tagsField;

        private object titleField;

        private bool has_notesField;

        private bool has_commentsField;

        private string file_urlField;

        private string preview_urlField;

        private string sample_urlField;

        private ushort sample_heightField;

        private ushort sample_widthField;

        private string statusField;

        private byte post_lockedField;

        private bool has_childrenField;

        /// <remarks/>
        public uint Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string Created_at
        {
            get
            {
                return this.created_atField;
            }
            set
            {
                this.created_atField = value;
            }
        }

        /// <remarks/>
        public byte Score
        {
            get
            {
                return this.scoreField;
            }
            set
            {
                this.scoreField = value;
            }
        }

        /// <remarks/>
        public int Width
        {
            get
            {
                return this.widthField;
            }
            set
            {
                this.widthField = value;
            }
        }

        /// <remarks/>
        public int Height
        {
            get
            {
                return this.heightField;
            }
            set
            {
                this.heightField = value;
            }
        }

        /// <remarks/>
        public string md5
        {
            get
            {
                return this.md5Field;
            }
            set
            {
                this.md5Field = value;
            }
        }

        /// <remarks/>
        public string directory
        {
            get
            {
                return this.directoryField;
            }
            set
            {
                this.directoryField = value;
            }
        }

        /// <remarks/>
        public string image
        {
            get
            {
                return this.imageField;
            }
            set
            {
                this.imageField = value;
            }
        }

        /// <remarks/>
        public string rating
        {
            get
            {
                return this.ratingField;
            }
            set
            {
                this.ratingField = value;
            }
        }

        /// <remarks/>
        public string source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        public uint change
        {
            get
            {
                return this.changeField;
            }
            set
            {
                this.changeField = value;
            }
        }

        /// <remarks/>
        public string owner
        {
            get
            {
                return this.ownerField;
            }
            set
            {
                this.ownerField = value;
            }
        }

        /// <remarks/>
        public uint creator_id
        {
            get
            {
                return this.creator_idField;
            }
            set
            {
                this.creator_idField = value;
            }
        }

        /// <remarks/>
        public byte parent_id
        {
            get
            {
                return this.parent_idField;
            }
            set
            {
                this.parent_idField = value;
            }
        }

        /// <remarks/>
        public byte sample
        {
            get
            {
                return this.sampleField;
            }
            set
            {
                this.sampleField = value;
            }
        }

        /// <remarks/>
        public byte preview_height
        {
            get
            {
                return this.preview_heightField;
            }
            set
            {
                this.preview_heightField = value;
            }
        }

        /// <remarks/>
        public byte preview_width
        {
            get
            {
                return this.preview_widthField;
            }
            set
            {
                this.preview_widthField = value;
            }
        }

        /// <remarks/>
        public string tags
        {
            get
            {
                return this.tagsField;
            }
            set
            {
                this.tagsField = value;
            }
        }

        /// <remarks/>
        public object title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public bool has_notes
        {
            get
            {
                return this.has_notesField;
            }
            set
            {
                this.has_notesField = value;
            }
        }

        /// <remarks/>
        public bool has_comments
        {
            get
            {
                return this.has_commentsField;
            }
            set
            {
                this.has_commentsField = value;
            }
        }

        /// <remarks/>
        public string File_url
        {
            get
            {
                return this.file_urlField;
            }
            set
            {
                this.file_urlField = value;
            }
        }

        /// <remarks/>
        public string Preview_url
        {
            get
            {
                return this.preview_urlField;
            }
            set
            {
                this.preview_urlField = value;
            }
        }

        /// <remarks/>
        public string Sample_url
        {
            get
            {
                return this.sample_urlField;
            }
            set
            {
                this.sample_urlField = value;
            }
        }

        /// <remarks/>
        public ushort sample_height
        {
            get
            {
                return this.sample_heightField;
            }
            set
            {
                this.sample_heightField = value;
            }
        }

        /// <remarks/>
        public ushort sample_width
        {
            get
            {
                return this.sample_widthField;
            }
            set
            {
                this.sample_widthField = value;
            }
        }

        /// <remarks/>
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public byte post_locked
        {
            get
            {
                return this.post_lockedField;
            }
            set
            {
                this.post_lockedField = value;
            }
        }

        /// <remarks/>
        public bool has_children
        {
            get
            {
                return this.has_childrenField;
            }
            set
            {
                this.has_childrenField = value;
            }
        }
    }
}