﻿using Telly.Abstractions.Models;

namespace Telly.Plugins.Booru
{
    internal class Rule34BooruCommandHandler : BooruCommandHandlerBase<Posts, Post>
    {
        public override string CommandName => "rule34";
        public override string Description => ";)";

        protected override string Url => "https://rule34.xxx/index.php?page=dapi&s=post&q=index";

        public override bool IsMatch(Command command) => command.Name == CommandName || command.Name == "rb";
    }
}