﻿using Telly.Abstractions.Attributes;

namespace Telly.Plugins.Booru
{
    [Setting("Plugins", "Booru")]
    public class BooruSettings
    {
        public string GelbooruApiKey { get; set; }
        public string GelbooruUserId { get; set; }
    }
}
