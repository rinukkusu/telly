﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;

namespace Telly.Plugins.Booru
{
    public class BooruMore
    {
        public IList<IPost> Posts { get; }
        public int Offset { get; private set; } = 0;

        public BooruMore(IList<IPost> posts)
        {
            Posts = posts;
        }

        public void Increase() => Offset += 5;
    }

    internal class BooruMoreCommandHandler : ICommandHandler
    {
        public string CommandName => "more";
        public string Description => "mooaarrr";

        public IList<IArgument> Arguments => new List<IArgument>();

        public bool IsMatch(Command command) => command.Name == CommandName;

        private static readonly IDictionary<dynamic, BooruMore> _history = new Dictionary<dynamic, BooruMore>();

        public static void SetHistory(dynamic userId, IList<IPost> posts)
        {
            if (_history.ContainsKey(userId))
                _history[userId] = new BooruMore(posts);
            else
                _history.Add(userId, new BooruMore(posts));
        }

        public Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            if (!_history.ContainsKey(command.OriginalMessage.Sender.Id))
                throw new Exception($"No Booru History for {command.OriginalMessage.Sender.Name} :O");

            BooruMore booruHistory = _history[command.OriginalMessage.Sender.Id];
            booruHistory.Increase();
            var postsToSend = booruHistory.Posts
                .Skip(booruHistory.Offset)
                .Take(5)
                .Select(x => x.File_url)
                .ToArray();

            if (!postsToSend.Any())
                throw new Exception("Nothing found.");

            IHandlerResult result = new DataCollectionsFromUrlsResult(postsToSend, Abstractions.Enums.DataType.Photo, null, true);

            return Task.FromResult(result);
        }
    }
}