using System;
using System.Collections.Generic;
using System.Linq;
using Telly.Abstractions;
using Telly.Abstractions.Models;

namespace Telly.Extensions
{
    public static class CommandHandlerExtensions
    {
        private static readonly IList<char> Quotes = new List<char>(){'"', '\''};

        public class ArgumentDto
        {
            public string Value { get; }
            public int Index { get; }
            public int Length { get; }

            public ArgumentDto(string value, int index, int length)
            {
                Value = value;
                Index = index;
                Length = length;
            }
        }
        
        public static IEnumerable<ArgumentDto> SplitArguments(string arguments)
        {
            var argument = "";
            var inQuotes = -1;
            var index = -1;

            foreach (var character in arguments)
            {
                index++;
                
                if (Quotes.Contains(character))
                {
                    if (inQuotes < 0)
                    {
                        inQuotes = Quotes.IndexOf(character);
                        continue;
                    }
                    else if (Quotes.IndexOf(character) == inQuotes)
                    {
                        inQuotes = -1;
                        yield return new ArgumentDto(argument, index-argument.Length, argument.Length + 1);
                        argument = "";
                        continue;
                    }
                }
                else
                {
                    if (inQuotes < 0 && character == ' ' && !argument.IsNullOrWhiteSpace())
                    {
                        yield return new ArgumentDto(argument, index-argument.Length, argument.Length);
                        argument = "";
                        continue;
                    }
                }

                if (character == ' ' && inQuotes < 0)
                    continue;
                
                argument += character;
            }

            if (!argument.IsNullOrWhiteSpace())
            {
                yield return new ArgumentDto(argument, index-argument.Length, argument.Length);
            }
        }
        
        public static IDictionary<string, dynamic> ParseArguments(this ICommandHandler handler, string arguments)
        {
            var index = 0;
            var argumentList = SplitArguments(arguments).ToList();
            var result = new Dictionary<string, dynamic>(handler.Arguments.Count);
            
            foreach (var handlerArgument in handler.Arguments)
            {
                dynamic value = null;
                
                if (handlerArgument is CatchAllArgument)
                {
                    var currentArguments = argumentList.Skip(index).ToList();
                    value = arguments.Substring(currentArguments.First().Index).Trim();
                }
                else
                {
                    var stringValue = argumentList.Skip(index).First().Value;
                    try
                    {
                        value = handlerArgument.Parse(stringValue);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Couldn't parse argument {index} ({handlerArgument.Name}) as {handlerArgument.GetType().Name} -> {ex.Message}");
                    }
                }

                if (!handlerArgument.IsOptional && value == null)
                {
                    throw new Exception($"{handlerArgument.Name} is not optional!");
                }
                
                result.Add(handlerArgument.Name, value);
                
                index++;
            }

            return result;
        }
    }
}
