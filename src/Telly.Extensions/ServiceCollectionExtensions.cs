using System;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Abstractions;
using Telly.Abstractions.Attributes;

namespace Telly.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSettings<TSettings>(this IServiceCollection services, IConfiguration configuration)
            where TSettings : class, new()
        {
            var attribute = typeof(TSettings).GetCustomAttribute<SettingAttribute>();
            if (attribute.File != null)
            {
                configuration = new ConfigurationBuilder()
                    .AddJsonFile(attribute.File)
                    .Build();
            }
            var configSection = configuration;
            if (attribute.NodePath != null && attribute.NodePath.Length > 0)
            {
                foreach (var nodePart in attribute.NodePath)
                {
                    configSection = configSection.GetSection(nodePart);
                }
            }
            
            var settings = new TSettings();
            configSection.Bind(settings);

            services.AddSingleton<TSettings>(settings);
            
            return services;
        }

        public static IServiceCollection AddManagedSettings<TSettings>(this IServiceCollection services) 
            where TSettings : class, new()
        {
            var attribute = typeof(TSettings).GetCustomAttribute<ManagedSettingAttribute>();
            if (attribute == null)
                throw new Exception($"{typeof(TSettings)} needs the [ManagedSetting] attribute defined on the class.");
            
            if (attribute.File.IsNullOrWhiteSpace())
                throw new Exception($"The settings file path needs to be specified for {typeof(TSettings)}");

            services.AddSingleton<SettingsManager<TSettings>>(new SettingsManager<TSettings>(attribute.File));

            return services;
        }

        public static IServiceCollection AddBotClient<TBotClient>(this IServiceCollection services) 
            where TBotClient : class, IBotClient
        {
            services.AddSingleton<IBotClient, TBotClient>();

            return services;
        }

        public static IServiceCollection AddCommandHandler<THandler>(this IServiceCollection services)
            where THandler : class, ICommandHandler
        {
            services.AddSingleton<ICommandHandler, THandler>();

            return services;
        }
        
        public static IServiceCollection AddMessageFilter<TFilter>(this IServiceCollection services)
            where TFilter : class, IMessageFilter
        {
            services.AddSingleton<IMessageFilter, TFilter>();

            return services;
        }
    }
}