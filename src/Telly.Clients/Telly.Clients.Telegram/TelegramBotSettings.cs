using Telly.Abstractions.Attributes;

namespace Telly.Clients.Telegram
{
    [Setting("Clients", "Telegram")]
    public class TelegramBotSettings
    {
        public string Token { get; set; }
        public int OwnerId { get; set; }
        public string BotName { get; set; }
    }
}