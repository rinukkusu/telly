using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using ExternalTelegramBotClient = Telegram.Bot.TelegramBotClient;
using MessageDto = Telly.Abstractions.Models.Message;
using UserDto = Telly.Abstractions.Models.User;
using ChatDto = Telly.Abstractions.Models.Chat;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Extensions.Polling;

namespace Telly.Clients.Telegram
{
    public class TelegramBotClient : BotClientBase, IBotClient
    {
        private readonly TelegramBotSettings _settings;
        private readonly ExternalTelegramBotClient _telegramBotClient;

        public override string CommandPrefix => "/";

        private readonly IReadOnlyDictionary<ParseMessageAs, ParseMode> _parseModeMap = new Dictionary<ParseMessageAs, ParseMode>()
        {
            { ParseMessageAs.Default, ParseMode.Html },
            { ParseMessageAs.Html, ParseMode.Html },
            { ParseMessageAs.Markdown, ParseMode.Markdown }
        };

        public override dynamic BotOwnerId => _settings.OwnerId;

        public TelegramBotClient(TelegramBotSettings settings)
        {
            _settings = settings;
            _telegramBotClient = new ExternalTelegramBotClient(settings.Token);
        }

        public override Task Start()
        {
            Console.WriteLine("Starting Telegram Client ...");

            _telegramBotClient.StartReceiving(UpdateHandler, ErrorHandler, new ReceiverOptions()
            {
                AllowedUpdates = new[]
                {
                    UpdateType.Message
                }
            });
            return Task.CompletedTask;
        }

        private Task ErrorHandler(ITelegramBotClient arg1, Exception arg2, CancellationToken arg3)
        {
            throw new NotImplementedException();
        }

        private Task UpdateHandler(ITelegramBotClient bot, Update args, CancellationToken cancellationToken)
        {
            OnMessage?.Invoke(this, new MessageDto()
            {
                Platform = "Telegram",
                Id = args.Message.MessageId,
                Date = args.Message.Date,
                Sender = new UserDto()
                {
                    Id = args.Message.From.Id,
                    Name = args.Message.From.Username
                           ?? $"{args.Message.From.FirstName} {args.Message.From.LastName}"
                },
                Target = new ChatDto()
                {
                    Id = args.Message.Chat.Id,
                    Name = args.Message.Chat.Title
                           ?? args.Message.Chat.Username
                           ?? $"{args.Message.From.FirstName} {args.Message.From.LastName}"
                },
                Text = args.Message.Text,
                HasMentionedBot = (args.Message.Text?.Contains(_settings.BotName) ?? false) ||
                                  args.Message.ReplyToMessage?.From?.Username == _settings.BotName,
                Mentions = args.Message.Entities
                    ?.Select(x => x.User?.Id)
                    ?.Cast<dynamic>()
                    ?.ToList()
            });

            return Task.CompletedTask;
        }

        public override event EventHandler<MessageDto> OnMessage;

        public override async Task SendTextAsync(MessageDto originalMessage, IEntity target, string text, ParseMessageAs parseMessageAs = ParseMessageAs.Default,
            dynamic replyToMessageId = null, bool disablePreview = false)
        {
            await _telegramBotClient.SendTextMessageAsync(
                new ChatId(target.Id),
                text,
                _parseModeMap[parseMessageAs],
                null,
                disablePreview,
                false,
                (int?)replyToMessageId ?? 0);
        }

        public override async Task SendDataAsync(MessageDto originalMessage, IEntity target, Stream data, DataType type, dynamic replyToMessageId = null)
        {
            replyToMessageId = replyToMessageId ?? 0;

            switch (type)
            {
                case DataType.File:
                    await _telegramBotClient.SendDocumentAsync(new ChatId(target.Id), new InputOnlineFile(data, "file"), null, null, ParseMode.Markdown, null, false, false, (int?)replyToMessageId);
                    break;

                case DataType.Photo:
                    await _telegramBotClient.SendPhotoAsync(new ChatId(target.Id), new InputOnlineFile(data, "file"), null, ParseMode.Markdown, null, false, (int?)replyToMessageId);
                    break;

                case DataType.Audio:
                    await _telegramBotClient.SendAudioAsync(new ChatId(target.Id), new InputOnlineFile(data, "file"), null, ParseMode.Markdown, null, 0, null, null, null, false, (int?)replyToMessageId);
                    break;

                case DataType.Video:
                    await _telegramBotClient.SendVideoAsync(new ChatId(target.Id), new InputOnlineFile(data, "file"), 0, 0, 0, null, null, ParseMode.Markdown, null, true, false, (int?)replyToMessageId);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public override async Task SendDataFromUrlAsync(MessageDto originalMessage, IEntity target, string dataUrl, DataType type, dynamic replyToMessageId = null)
        {
            replyToMessageId = replyToMessageId ?? 0;

            switch (type)
            {
                case DataType.File:
                    await _telegramBotClient.SendDocumentAsync(new ChatId(target.Id), new InputMedia(dataUrl), null, null, ParseMode.Markdown, null, false, false, (int?)replyToMessageId);
                    break;

                case DataType.Photo:
                    await _telegramBotClient.SendPhotoAsync(new ChatId(target.Id), new InputMedia(dataUrl), null, ParseMode.Markdown, null, false, (int?)replyToMessageId);
                    break;

                case DataType.Audio:
                    await _telegramBotClient.SendAudioAsync(new ChatId(target.Id), new InputMedia(dataUrl), null, ParseMode.Markdown, null, 0, null, null, null, false, (int?)replyToMessageId);
                    break;

                case DataType.Video:
                    await _telegramBotClient.SendVideoAsync(new ChatId(target.Id), new InputMedia(dataUrl), 0, 0, 0, null, null, ParseMode.Markdown, null, true, false, (int?)replyToMessageId);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public override async Task SendDataCollectionFromUrlsAsync(MessageDto originalMessage, IEntity target, string[] dataUrls, DataType type, dynamic replyToMessageId = null)
        {
            replyToMessageId = replyToMessageId ?? 0;

            switch (type)
            {
                case DataType.File:
                    await _telegramBotClient.SendMediaGroupAsync(new ChatId(target.Id), dataUrls.Select(x => new InputMediaDocument(new InputMedia(x))), false, (int?)replyToMessageId);
                    break;

                case DataType.Photo:
                    await _telegramBotClient.SendMediaGroupAsync(new ChatId(target.Id), dataUrls.Select(x => new InputMediaPhoto(new InputMedia(x))), false, (int?)replyToMessageId);
                    break;

                case DataType.Audio:
                    await _telegramBotClient.SendMediaGroupAsync(new ChatId(target.Id), dataUrls.Select(x => new InputMediaAudio(new InputMedia(x))), false, (int?)replyToMessageId);
                    break;

                case DataType.Video:
                    await _telegramBotClient.SendMediaGroupAsync(new ChatId(target.Id), dataUrls.Select(x => new InputMediaVideo(new InputMedia(x))), false, (int?)replyToMessageId);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}