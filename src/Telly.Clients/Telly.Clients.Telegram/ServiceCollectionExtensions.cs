using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;

namespace Telly.Clients.Telegram
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddTelegram(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSettings<TelegramBotSettings>(configuration);
            services.AddBotClient<TelegramBotClient>();

            return services;
        }
    }
}