﻿using GravyIrc;
using GravyIrc.Connection;
using GravyIrc.Messages;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Enums;
using Telly.Abstractions.Models;
using IrcUser = GravyIrc.User;

namespace Telly.Clients.IRC
{
    public class IrcBotClient : BotClientBase
    {
        public override string CommandPrefix => _settings.CommandPrefix;
        public override dynamic BotOwnerId => throw new NotImplementedException();

        public override event EventHandler<Message> OnMessage;

        private readonly IrcBotSettings _settings;
        private readonly IrcClient _ircClient;
        private readonly IrcServerSettings _ircServer;

        public IrcBotClient(IrcBotSettings settings)
        {
            _settings = settings;
            _ircServer = _settings.Servers.FirstOrDefault();
            var user = new IrcUser(_settings.Username, _settings.Realname);
            var conn = new TcpClientConnection();
            _ircClient = new IrcClient(user, conn);

            _ircClient.EventHub.Subscribe<PrivateMessage>((_, message) =>
            {
                OnMessage?.Invoke(this, new Message
                {
                    Platform = "IRC",
                    Id = null,
                    Date = DateTime.Now,
                    Sender = new Abstractions.Models.User { Id = message.IrcMessage.From, Name = message.IrcMessage.From },
                    Target = new Abstractions.Models.Chat { Id = message.IrcMessage.To, Name = message.IrcMessage.To },
                    Text = message.IrcMessage.Message
                });
            });

            _ircClient.EventHub.Subscribe<RplWelcomeMessage>(async (_, message) =>
            {
                foreach (var channel in _ircServer.Channels)
                {
                    await _ircClient.SendAsync(new JoinMessage(channel));
                }
            });

            _ircClient.OnRawDataReceived += (client, data) => Console.WriteLine(data);
        }

        public override async Task Start()
        {
            Console.WriteLine("Starting IRC Client ...");

            await _ircClient.ConnectAsync(_ircServer.Hostname, _ircServer.Port ?? 6667);
        }

        private IEntity GetCorrectTarget(Message originalMessage, IEntity target) => target.Id == _settings.Username ? originalMessage.Sender : target;

        public override Task SendDataAsync(Message originalMessage, IEntity target, Stream data, DataType type, dynamic replyToMessageId = null)
        {
            target = GetCorrectTarget(originalMessage, target);
            throw new NotImplementedException();
        }

        public override async Task SendDataCollectionFromUrlsAsync(Message originalMessage, IEntity target, string[] dataUrls, DataType type, dynamic replyToMessageId = null)
        {
            target = GetCorrectTarget(originalMessage, target);
            await _ircClient.SendAsync(new PrivateMessage(target.Id, string.Join(",", dataUrls)));
        }

        public override async Task SendDataFromUrlAsync(Message originalMessage, IEntity target, string dataUrl, DataType type, dynamic replyToMessageId = null)
        {
            target = GetCorrectTarget(originalMessage, target);
            await _ircClient.SendAsync(new PrivateMessage(target.Id, dataUrl));
        }

        public override async Task SendTextAsync(Message originalMessage, IEntity target, string text, ParseMessageAs parseMessageAs = ParseMessageAs.Default, dynamic replyToMessageId = null, bool disablePreview = false)
        {
            target = GetCorrectTarget(originalMessage, target);
            var partialMessages = text.Split("\n");
            foreach (var partialMessage in partialMessages)
            {
                await _ircClient.SendAsync(new PrivateMessage(target.Id, partialMessage));
            }
        }
    }
}
