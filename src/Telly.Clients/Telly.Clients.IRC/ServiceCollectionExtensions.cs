using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Extensions;

namespace Telly.Clients.IRC
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddIrc(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSettings<IrcBotSettings>(configuration);
            services.AddBotClient<IrcBotClient>();

            return services;
        }
    }
}