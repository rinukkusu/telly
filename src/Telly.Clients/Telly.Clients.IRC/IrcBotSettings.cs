﻿using System;
using System.Collections.Generic;
using System.Text;
using Telly.Abstractions.Attributes;

namespace Telly.Clients.IRC
{
    [Setting("Clients", "IRC")]
    public class IrcBotSettings
    {
        public string CommandPrefix { get; set; }
        public string Username { get; set; }
        public string Realname { get; set; }
        public string Password { get; set; }
        public IrcServerSettings[] Servers { get; set; }
    }

    public class IrcServerSettings
    {
        public string Hostname { get; set; }
        public short? Port { get; set; }
        public string[] Channels { get; set; }
    }
}
