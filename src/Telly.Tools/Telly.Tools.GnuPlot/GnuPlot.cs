﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Telly.Tools
{
    public class GnuPlot
    {
        private readonly IList<string> _commands = new List<string>();

        public GnuPlot()
        {

        }

        protected GnuPlot(IList<string> commands)
        {
            _commands = commands;
        }

        public static GnuPlot FromExisting(GnuPlot gnuPlot)
        {
            return new GnuPlot(gnuPlot._commands.ToList());
        }

        public GnuPlot Set(string key, string arguments = null)
        {
            _commands.Add($"set {key}{(arguments != null ? $" {arguments}" : string.Empty)}");
            return this;
        }

        public GnuPlot Unset(string key, string arguments = null)
        {
            _commands.Add($"unset {key}{(arguments != null ? $" {arguments}" : string.Empty)}");
            return this;
        }

        public GnuPlot SetQuoted(string key, string arguments) =>
            Set(key, $"'{arguments}'");

        public GnuPlot SetTerminal(string terminal, int width = 640, int height = 480) =>
            Set("terminal", $"{terminal} size {width},{height}");

        public GnuPlot SetTimeFormat(string format) =>
            SetQuoted("timefmt", format);

        public GnuPlot SetXData(string type) =>
            Set("xdata", type);

        public GnuPlot SetYData(string type) =>
            Set("ydata", type);

        public GnuPlot SetTitle(string title) =>
            SetQuoted("title", title);

        public GnuPlot SetStyle(params string[] styles) =>
            Set("style", string.Join(" ", styles));

        public async Task<Image> Plot(string inputFile, string options = null)
        {
            options = options != null ? $" {options}" : string.Empty;

            var gnuPlot = new Process
            {
                StartInfo =
                {
                    RedirectStandardError = true, 
                    RedirectStandardOutput = true, 
                    FileName = "gnuplot",
                    Arguments = $" -e \"{BuildArguments()}; plot '{inputFile}'{options}\""
                }
            };

            gnuPlot.Start();
            gnuPlot.WaitForExit();

            var error = await gnuPlot.StandardError.ReadToEndAsync();
            if (!string.IsNullOrEmpty(error))
                throw new Exception($"GnuPlot Error - {error}");

            var mem = new MemoryStream();
            var fs = gnuPlot.StandardOutput.BaseStream as FileStream;

            var buffer = new byte[4096];
            int lastRead;
            do
            {
                lastRead = fs.Read(buffer, 0, buffer.Length);
                mem.Write(buffer, 0, lastRead);
            } while (lastRead > 0);

            mem.Seek(0, SeekOrigin.Begin);

            return Image.FromStream(mem);
        }

        private string BuildArguments()
        {
            return string.Join(";", _commands);
        }
    }
}
