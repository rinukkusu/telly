﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telly.Tools.MongoDb;

namespace Telly.Tools.ChatExplorer
{
    public static class ServiceSetup
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMongoDb(configuration.GetConnectionString("MongoDb"));

            return services;
        }
    }
}