﻿using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Telly.Tools.MongoDb.Models;

namespace Telly.Tools.MongoDb
{
    public class SearchRepository
    {
        private readonly MongoRepository<SearchEntry> _repository;
        public SearchRepository(IMongoClient mongoClient)
        {
            _repository = new MongoRepository<SearchEntry>(mongoClient, "telegram", "_searches");
        }

        public Task<SearchEntry> GetAsync(ObjectId id)
        {
            return _repository
                .Find(x => x.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<ObjectId> CreateSearchEntry(SearchEntry searchEntry)
        {
            await _repository.AddAsync(searchEntry);
            return searchEntry.Id;
        }
    }
}