﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Telly.Tools.MongoDb.Models;

namespace Telly.Tools.MongoDb
{
    public class MessageRepository
    {
        private readonly MongoRepository<TelegramMessage> _repository;
        public MessageRepository(IMongoClient mongoClient, string collection) 
        {
            _repository = new MongoRepository<TelegramMessage>(mongoClient, "telegram", collection);
        }

        public Task<TelegramMessage> GetAsync(ObjectId id)
        {
            return _repository
                .Find(x => x.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<IList<TelegramMessage>> FindMessagesAsync(string keywordString)
        {
            var keywords = keywordString.Split(" ");

            var messages = (await _repository
                .Find(x => x.Text.Contains(keywords[0]) && x.Type == "message")
                .ToListAsync())
                .Where(x => x.Poll == null);

            if (keywords.Length > 1)
            {
                for (var i = 1; i < keywords.Length; i++)
                {
                    var keyword = keywords[i];
                    messages = keyword.StartsWith("-")
                        ? messages.Where(x => !x.Text.Contains(keyword.Remove(0, 1)))
                        : messages.Where(x => x.Text.Contains(keyword));
                }
            }

            return messages.ToList();
        }

        public async Task<IList<TelegramMessage>> GetRangeAsync(int messageId, int range = 20)
        {
            return await _repository
                .Find(x => x.Type == "message" 
                           && x.MessageId >= messageId - range
                           && x.MessageId <= messageId + range)
                .ToListAsync();
        }

        public async Task<IList<TelegramMessage>> GetNextAsync(int messageId, int count = 20)
        {
            return await _repository
                .Find(x => x.Type == "message" && x.MessageId > messageId)
                .Limit(count)
                .ToListAsync();
        }

        public async Task<IList<TelegramMessage>> GetPreviousAsync(int messageId, int count = 20)
        {
            var messages = await _repository
                .Find(x => x.Type == "message"
                           && x.MessageId < messageId)
                .SortByDescending(x => x.MessageId)
                .Limit(count)
                .ToListAsync();

            return messages
                .OrderBy(x => x.MessageId)
                .ToList();
        }
    }
}