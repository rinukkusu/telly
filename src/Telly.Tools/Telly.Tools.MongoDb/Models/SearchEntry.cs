﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Telly.Tools.MongoDb.Models
{
    public class SearchEntry
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("date")] public DateTime DateTime { get; set; }
        [BsonElement("expiresIn")] public int ExpiresInSeconds { get; set; }
        [BsonElement("from")] public string From { get; set; }
        [BsonElement("from_id")] public int FromId { get; set; }
        [BsonElement("keywords")] public string Keywords { get; set; }

        [BsonIgnore] public bool IsExpired => (DateTime + TimeSpan.FromSeconds(ExpiresInSeconds)) <= DateTime.UtcNow;
    }
}