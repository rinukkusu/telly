﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Telly.Tools.MongoDb.Models
{
    public class TelegramMessage
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }
        [BsonElement("id")] public int MessageId { get; set; }
        [BsonElement("reply_to_message_id")] public int? ReplyToMessageId { get; set; }
        [BsonElement("type")] public string Type { get; set; }
        [BsonElement("date")] public DateTime Date { get; set; }
        [BsonElement("edited")] public DateTime EditedOn { get; set; }
        [BsonElement("actor")] public string Actor { get; set; }
        [BsonElement("actor_id")] public string ActorId { get; set; }
        [BsonElement("action")] public string Action { get; set; }
        [BsonElement("photo")] public string Photo { get; set; }
        [BsonElement("from")] public string From { get; set; }
        [BsonElement("forwarded_from")] public string ForwardedFrom { get; set; }
        [BsonElement("from_id")] public int? FromId { get; set; }
        [BsonElement("file")] public string File { get; set; }
        [BsonElement("thumbnail")] public string Thumbnail { get; set; }
        [BsonElement("media_type")] public string MediaType { get; set; }
        [BsonElement("mime_type")] public string MimeType { get; set; }
        [BsonElement("duration_seconds")] public int? DurationSeconds { get; set; }
        [BsonElement("sticker_emoji")] public string StickerEmoji { get; set; }
        [BsonElement("width")] public int? Width { get; set; }
        [BsonElement("height")] public int? Height { get; set; }
        [BsonElement("text")] public string Text { get; set; }
        [BsonElement("performer")] public string Performer { get; set; }
        [BsonElement("title")] public string Title { get; set; }
        [BsonElement("via_bot")] public string ViaBot { get; set; }
        [BsonElement("poll")] public BsonDocument Poll { get; set; }
    }
}