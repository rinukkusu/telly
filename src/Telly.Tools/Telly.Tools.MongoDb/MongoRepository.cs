﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Telly.Tools.MongoDb
{
    public class MongoRepository<T>
    {
        protected readonly IMongoCollection<T> Collection;

        public MongoRepository(IMongoClient mongoClient, string database, string collection)
        {
            Collection = mongoClient
                .GetDatabase(database)
                .GetCollection<T>(collection);
        }

        public Task AddAsync(T document) => Collection.InsertOneAsync(document);
        public Task AddAsync(IEnumerable<T> documents) => Collection.InsertManyAsync(documents);
        public virtual IFindFluent<T,T> Find(Expression<Func<T, bool>> filter) => Collection.Find(filter);
        public virtual IFindFluent<T, T> Find(BsonDocument filter) => Collection.Find(filter);
        public Task DeleteAsync(Expression<Func<T, bool>> filter) => Collection.DeleteManyAsync(filter);
    }
}
