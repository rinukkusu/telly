﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace Telly.Tools.MongoDb
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMongoDb(this IServiceCollection services, string connectionString)
        {
            services.AddScoped<IMongoClient>(_ => new MongoClient(connectionString));
            services.AddScoped<SearchRepository>();

            return services;
        }
    }
}