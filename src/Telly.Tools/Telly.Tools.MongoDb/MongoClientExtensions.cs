﻿using MongoDB.Driver;

namespace Telly.Tools.MongoDb
{
    public static class MongoClientExtensions
    {
        public static MongoRepository<T> GetRepository<T>(this IMongoClient mongoClient, string database, string collection)
        {
            return new MongoRepository<T>(mongoClient, database, collection);
        }
    }
}