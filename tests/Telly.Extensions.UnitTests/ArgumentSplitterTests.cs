using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Telly.Extensions.UnitTests
{
    [TestClass]
    public class ArgumentSplitterTests
    {
        [TestMethod]
        public void SimpleArguments()
        {
            var args = CommandHandlerExtensions.SplitArguments("hallo wie gehts").ToList();
            
            Assert.AreEqual(3, args.Count);
            Assert.AreEqual("hallo", args[0].Value);
            Assert.AreEqual("wie", args[1].Value);
            Assert.AreEqual("gehts", args[2].Value);
        }
        
        [TestMethod]
        public void QuotedArguments()
        {
            var args = CommandHandlerExtensions.SplitArguments("hallo \"wie gehts\" dir").ToList();
            
            Assert.AreEqual(3, args.Count);
            Assert.AreEqual("hallo", args[0].Value);
            Assert.AreEqual("wie gehts", args[1].Value);
            Assert.AreEqual("dir", args[2].Value);
        }

        [TestMethod]
        public void SpaceInQuotesVsNotInQuotes()
        {
            var args = CommandHandlerExtensions.SplitArguments(" hallo    \"wie gehts \"   dir ").ToList();
            
            Assert.AreEqual(3, args.Count);
            Assert.AreEqual("hallo", args[0].Value);
            Assert.AreEqual("wie gehts ", args[1].Value);
            Assert.AreEqual("dir", args[2].Value);
        }

        [TestMethod]
        public void SingleQuotesInDoubleQuotes()
        {
            var args = CommandHandlerExtensions.SplitArguments("hallo \"wie 'gehts'\" dir").ToList();
            
            Assert.AreEqual(3, args.Count);
            Assert.AreEqual("hallo", args[0].Value);
            Assert.AreEqual("wie 'gehts'", args[1].Value);
            Assert.AreEqual("dir", args[2].Value);
        }
        
        [TestMethod]
        public void DoubleQuotesInSingleQuotes()
        {
            var args = CommandHandlerExtensions.SplitArguments("hallo 'wie \"gehts\"' dir").ToList();
            
            Assert.AreEqual(3, args.Count);
            Assert.AreEqual("hallo", args[0].Value);
            Assert.AreEqual("wie \"gehts\"", args[1].Value);
            Assert.AreEqual("dir", args[2].Value);
        }
        
        [TestMethod]
        public void EmptyQuotes()
        {
            var args = CommandHandlerExtensions.SplitArguments("'' '' ''").ToList();
            
            Assert.AreEqual(3, args.Count);
            Assert.AreEqual("", args[0].Value);
            Assert.AreEqual("", args[1].Value);
            Assert.AreEqual("", args[2].Value);
        }
    }
}