using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telly.Abstractions;
using Telly.Abstractions.Models;

namespace Telly.Extensions.UnitTests
{
    public class TestCommandHandler : ICommandHandler
    {
        public string CommandName { get; }
        public string Description { get; }
        public IList<IArgument> Arguments { get; set; }
        public bool IsMatch(Command command)
        {
            return true;
        }

        public Task<IHandlerResult> Handle(IBotClient client, Command command)
        {
            return Task.FromResult((IHandlerResult) new EmptyResult());
        }

        public static ICommandHandler WithArguments(params IArgument[] arguments)
        {
            return new TestCommandHandler()
            {
                Arguments = arguments.ToList()
            };
        }
    }
}