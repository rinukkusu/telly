﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Telly.Abstractions.Attributes;

namespace Telly.Extensions.UnitTests
{
    [Setting("Plugins", "Test")]
    public class TestSettings
    {
        public string Value { get; set; }
        public int Number { get; set; }
    }

    [Setting("Plugins", "Test", File = "SettingsTests/appsettings.test.json")]
    public class NamedSettings : TestSettings
    {
    }

    [TestClass]
    public class SettingsTests
    {
        [TestMethod]
        public void SettingsTest()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile(Path.Join("SettingsTests", "appsettings.test.json"))
                .Build();

            var services = new ServiceCollection()
                .AddSettings<TestSettings>(config)
                .BuildServiceProvider();

            var settings = services.GetService<TestSettings>();

            Assert.IsNotNull(settings);
            Assert.AreEqual("Test", settings.Value);
            Assert.AreEqual(42, settings.Number);
        }

        [TestMethod]
        public void NamedSettingsTest()
        {
            var config = new ConfigurationBuilder()
                .Build();

            var services = new ServiceCollection()
                .AddSettings<NamedSettings>(config)
                .BuildServiceProvider();

            var settings = services.GetService<NamedSettings>();

            Assert.IsNotNull(settings);
            Assert.AreEqual("Test", settings.Value);
            Assert.AreEqual(42, settings.Number);
        }
    }

    
}
