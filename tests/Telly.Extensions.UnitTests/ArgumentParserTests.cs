using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Telly.Abstractions.Models;

namespace Telly.Extensions.UnitTests
{
    [TestClass]
    public class ArgumentParserTests
    {
        [TestMethod]
        public void TextArgument()
        {
            var handler = TestCommandHandler.WithArguments(new TextArgument("test"));

            var args = handler.ParseArguments("hallo");

            Assert.AreEqual(handler.Arguments.Count, args.Count);
            Assert.IsTrue(args.ContainsKey("test"));
            Assert.IsInstanceOfType(args["test"], typeof(string));
            Assert.AreEqual("hallo", args["test"]);
        }
        
        [TestMethod]
        public void MultipleTextArguments()
        {
            var handler = TestCommandHandler.WithArguments(
                new TextArgument("testA"),
                new TextArgument("testB"));

            var args = handler.ParseArguments("hallo du");

            Assert.AreEqual(handler.Arguments.Count, args.Count);
            Assert.IsTrue(args.ContainsKey("testA"));
            Assert.IsTrue(args.ContainsKey("testB"));
            Assert.AreEqual("hallo", args["testA"]);
            Assert.AreEqual("du", args["testB"]);
        }
        
        [TestMethod]
        public void IntegerArgument()
        {
            var handler = TestCommandHandler.WithArguments(
                new IntArgument("test"));

            var args = handler.ParseArguments("21");

            Assert.AreEqual(handler.Arguments.Count, args.Count);
            Assert.IsTrue(args.ContainsKey("test"));
            Assert.IsInstanceOfType(args["test"], typeof(int));
            Assert.AreEqual(21, args["test"]);
        }
        
        [TestMethod]
        public void MultipleMixedArguments()
        {
            var handler = TestCommandHandler.WithArguments(
                new TextArgument("testA"),
                new IntArgument("testInt"),
                new ListArgument("testList", ","),
                new TextArgument("testB"),
                new CatchAllArgument("testCatchAll"));

            var args = handler.ParseArguments("hallo 21 a,b,c du a b c");

            Assert.AreEqual(handler.Arguments.Count, args.Count);
            Assert.IsTrue(args.ContainsKey("testA"));
            Assert.IsTrue(args.ContainsKey("testInt"));
            Assert.IsTrue(args.ContainsKey("testList"));
            Assert.IsTrue(args.ContainsKey("testB"));
            Assert.IsTrue(args.ContainsKey("testCatchAll"));
            Assert.AreEqual("hallo", args["testA"]);
            Assert.AreEqual(21, args["testInt"]);
            Assert.AreEqual(3, args["testList"].Count);
            Assert.AreEqual("du", args["testB"]);
            Assert.AreEqual("a b c", args["testCatchAll"]);
        }

        [TestMethod]
        public void ListArgument()
        {
            var handler = TestCommandHandler.WithArguments(
                new ListArgument("test", ","));

            var args = handler.ParseArguments("meine,coole,liste");
            
            Assert.AreEqual(handler.Arguments.Count, args.Count);
            Assert.IsTrue(args.ContainsKey("test"));
            Assert.IsInstanceOfType(args["test"], typeof(IEnumerable<string>));
            Assert.AreEqual("meine", args["test"][0]);
            Assert.AreEqual("coole", args["test"][1]);
            Assert.AreEqual("liste", args["test"][2]);
        }

        [TestMethod]
        public void CatchAllArgument()
        {
            var handler = TestCommandHandler.WithArguments(
                new CatchAllArgument("test"));

            var args = handler.ParseArguments("hallo wie gehts");
            
            Assert.AreEqual(handler.Arguments.Count, args.Count);
            Assert.IsTrue(args.ContainsKey("test"));
            Assert.IsInstanceOfType(args["test"], typeof(string));
            Assert.AreEqual(args["test"], "hallo wie gehts");
        }
    }
}